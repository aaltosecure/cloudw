Open-source wireless access point with cloud user interface
=============

This repository contains software to implement cloud controlled OpenWRT and DDwrt access points.

# Folders

* agent/ - Software agent for access points
* controller/ - Node.js cloud controller sofware
* docs/ - documentation
* setup/ - Installation files and scripts
