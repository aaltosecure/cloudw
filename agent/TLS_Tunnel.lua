        
        -- Lua program to create TLS tunnels between router and cloud

        local ssl = require("ssl")
        local socket = require("socket")                                 
        local server = socket.bind("192.168.1.1", 8080)                                                                                      
        local servers = {}                                               
        local clientslist = {}                                           
        server:settimeout(1)                                            
       
        --TLS Parameters to be used      

        local params = {                                         
            mode = "client",                                
                            protocol = "tlsv1",             
                            cafile = "/root/lua_api/certs/server.pem",
                            key = "/root/lua_api/certs/device1.key",
                            certificate = "/root/lua_api/certs/client-cert.pem",
                            verify = "peer",            
                            options = "all"                  
            } 

            -- Function to receive the data from one socket and send it to another socket

            function sendReceive( sockets, list )
                local index
                for i,client in ipairs(sockets) do                       
                    while (true) do      
                        -- Receiving data from one socket
                        local line, err, partial = client:receive(1500)

                        -- Loop to check from which client data is received in case of many clients                   
                            for j,object in ipairs(servers) do
                                if (object == client) then
                                    sentno = j
                                end
                            end

                            for j,object in ipairs(clientslist) do
                                if (object == client) then
                                    sentno = j
                                end
                            end

                        -- Send the data to respective clients considering all error conditions
                        if not line and not err and partial ~= ""  then
                            localtest = list[sentno]:send(partial)

                        elseif not err and not partial and line ~= "" then
                            localtest = list[sentno]:send(line)
                        
                        elseif not line and err == 'timeout' and  partial ~= ""  then
                            localtest = list[sentno]:send(partial)
                			break

                        elseif not line and err == 'closed' and  partial ~= ""  then
                            localtest = list[sentno]:send(partial)
                            index = sentno
                			break

                        elseif not partial and err == 'timeout' and  line ~= ""  then
                            localtest = list[sentno]:send(line)
    		              	break

                        elseif not partial and err == 'closed' and  line ~= ""  then
                            localtest = list[sentno]:send(line)
                            index = sentno
                			break

            		    elseif not line and err == 'wantread' and partial ~= ""  then
    			            localtest = list[sentno]:send(partial)
    			            break
    		            
                        elseif not partial and err == 'wantread' and line ~= "" then 
                            localtest = list[sentno]:send(line)         
                            break	
    		            
                        elseif not partial and err == 'closed' then
    			            localtest = list[sentno]:send(line)            
                            index = sentno                                 
                            break
        		        elseif not line and err == 'closed'  then
    			            localtest = list[sentno]:send(partial)            
                            index = sentno                                 
                            break 
        		        elseif not line and err == 'timeout'  then
    			            localtest = list[sentno]:send(partial)
    			            break
                        else
    			            print ("new condition")
                            break
                        end
            		end
                end
                return index
            end                                                
        
        -- main program starts here

        while(true) do             

            -- accepting the connection from client computerand creating respective TLS socket to connect with cloud                                     
            new_client = server:accept()                           
                if (new_client~=nil) then                              
                    table.insert(servers,new_client)               
                    new_client:settimeout(0.5)                      
                    local endpoint = socket.tcp()                  
                    endpoint:connect("130.233.193.174", 8081) 
                    	if (endpoint~=nil) then                        
                        	endpoint = ssl.wrap(endpoint, params)
                        	local succ, msg
                        		while not succ do
                            		succ, msg = endpoint:dohandshake()
                            		if msg == "wantread" then
                                			socket.select({endpoint}, nil)
                            		elseif msg == "wantwrite" then
                                			socket.select(nil, {endpoint})
                            		else
                               			 -- other errors
                           			end
    				            end
    				        endpoint:settimeout(1)
    				            if (endpoint~=nil)  then
    					           table.insert(clientslist,endpoint)
    					           --print ("Added TLS client")
    				            end
                       	end                                    
    		    end                                    

            -- socket select function to read data from server sockets
            local canread = socket.select(servers, nil, 1)           
                if (canread~=nil)    then       
                    indextoremove = sendReceive(canread,clientslist)
                end

                -- remove clients in case connection is closed
                if (indextoremove~=nil) then
                    table.remove(servers,indextoremove)
                    table.remove(clientslist,indextoremove)
                end
           
            -- socket select function to read data from client sockets
            local canwrite = socket.select(clientslist, nil, 1)                           
                if (canwrite~=nil)    then                                 
                    indextoremove = sendReceive(canwrite,servers)                                                  
                end 

                -- remove clients in case connection is closed
                if (indextoremove~=nil) then
                    table.remove(servers,indextoremove)
                    table.remove(clientslist,indextoremove)
                end                                                                          
        end
