require("uci")       
json = require( "dkjson" )
require("socket")
local https = require("ssl.https")
local ltn12 = require"ltn12"
local x=uci.cursor()
http = require("socket.http")


-- Function to loop over configuration value. It is used while accessing Lua Api to update/delete the key

function getConfType(conf,type)
   local curs=uci.cursor()
   local ifce={}
   local count=1
   curs:foreach(conf,type,function(s) ifce[count]=s count=count+1 end)
   return ifce
end

-- function to print the debug logging information

function debugLogging(debugInfo)
	
	if (configList["DEBUG_LOGGING_ENABLED"]=="1")  then
		print(debugInfo)
	end
end

-- File to be created for storing the router Id

function fileWrite(deviceId)
	debugLogging("debugInfo: Updating Device Id")
	file = io.open(configList["DEV_FILE_PATH"]..configList["DEV_ID_FILE"], "w")
    file:write(deviceId)
    file:close()
end

-- Function to read the router Id file and return the device Id

function fileRead()
	debugLogging("debugInfo: Reading Device Id")
	file = io.open(configList["DEV_FILE_PATH"]..configList["DEV_ID_FILE"], "r") 
	deviceId = file:read()
	file:close()
	return deviceId
end


-- Function to parse the string key such that it can be used by Lua API to insert the value

function parseValue( key )
	local l = 0
	local flag = 0
	local t = {}
	local index
	debugLogging("debugInfo: parsing each key to get the value")
	l = string.find(key,"@")
	if (l~=nil)			then
		flag = 1
		for i in string.gmatch(key, "%@*%w+_*%-*%w*%[*%d*]*_*%-*%w*%.-") do
			if ((string.find(i,"@"))~=nil) then
					index=string.sub(i, string.find(i, "%d"))
					string.gsub(i,"%a+%-*_*%a+",function(x) i = x end)
					table.insert(t,i)
			else
					table.insert(t,i)
			end
		end
	else
		for i in string.gmatch(key, "(%w+_*-*%w+)%.-") do
			table.insert(t,i)
			print (i)
		end
	end
	return flag,index,t
	-- body
end

-- Function to get config value of each key

function getConfig( key )
	debugLogging("debugInfo: Getting each value of key")
	local flag,index
	local t = {}
	-- calling function to parse the key value
	flag,index,t = parseValue(key)
	local length = table.getn(t) 
	if flag == 1 then
		if (length==2) then
			value = x:get(t[1],getConfType(t[1],t[2])[index+1][".name"])
		elseif (length==3) then 
			value = x:get(t[1],getConfType(t[1],t[2])[index+1][".name"],t[3])
		end 
	else
		if (length==2) then
			value = x:get(t[1],t[2])
		elseif (length==3) then
			value = x:get(t[1],t[2],t[3])
		end	
	end

	if (type(value)=="table")   then
		local jsonReturnValue = json.encode(value)
		return jsonReturnValue
	else
		return value
	end

--	return value
	-- body
end


--[[

function parseValues(key)
	local l = 0
	local t = {}
	local index
	l = string.find(key,"@")
	if (l~=nil)			then
		for i in string.gmatch(key, "%@*%w+_*%-*%w*%[*%d*]*_*%-*%w*%.-") do
			if ((string.find(i,"@"))~=nil) then
					index=string.sub(i, string.find(i, "%d"))
					string.gsub(i,"%a+%-*_*%a+",function(x) i = x end)
					table.insert(t,i)
			else
					table.insert(t,i)
			end
		end
		local length = table.getn(t)         
		if (length==2) then
			value = x:get(t[1],getConfType(t[1],t[2])[index+1][".name"])
		elseif (length==3) then 
			value = x:get(t[1],getConfType(t[1],t[2])[index+1][".name"],t[3])
		end   
	else
		for i in string.gmatch(key, "(%w+_*-*%w+)%.-") do
			table.insert(t,i)
			print (i)
		end
		local length = table.getn(t)
		if (length==2) then
			value = x:get(t[1],t[2])
		elseif (length==3) then
			value = x:get(t[1],t[2],t[3])
		end	
	end
	if (type(value)=="table")   then
		local jsonReturnValue = json.encode(value)
		print (jsoReturnValue)
		return jsonReturnValue
	else
		return value
	end
end

--]]

-- Function to update/add key/value pair

function updateKeyValue( key,value )
	print(value)
	
	local l = 0
	local t = {}
	local index
	debugLogging("debugInfo: Updating each key/value")
	l = string.find(key,"@")
	if (l~=nil)			then
		for i in string.gmatch(key, "%@*%w+_*%-*%w*%[*%d*]*_*%-*%w*%.-") do
			if ((string.find(i,"@"))~=nil) then
					index=string.sub(i, string.find(i, "%d"))
					string.gsub(i,"%a+%-*_*%a+",function(x) i = x end)
					table.insert(t,i)
			else
					table.insert(t,i)
			end
		end
		local length = table.getn(t)         
		if (length==2) then
			print("new index"..index)
			boolean = x:set(t[1],getConfType(t[1],t[2])[index+1][".name"],value)
		elseif (length==3) then 
			boolean = x:set(t[1],getConfType(t[1],t[2])[index+1][".name"],t[3],value)
		end   
	else
		for i in string.gmatch(key, "(%w+_*-*%w+)%.-") do
			table.insert(t,i)
		end
		local length = table.getn(t)
		if (length==2) then
			boolean = x:set(t[1],t[2],value)
		elseif (length==3) then
			boolean = x:set(t[1],t[2],t[3],value)
		end	
	end

	if (boolean==true) then
		x:save(t[1])
		x:commit(t[1])
	end
end

-- Function to delete each key value

function deleteKeyValue(key)
	local l = 0
	local t = {}
	local index
	debugLogging("debugInfo: Deleting each key/value")
	l = string.find(key,"@")
	if (l~=nil)			then
		for i in string.gmatch(key, "%@*%w+_*%-*%w*%[*%d*]*_*%-*%w*%.-") do
			if ((string.find(i,"@"))~=nil) then
					index=string.sub(i, string.find(i, "%d"))
					string.gsub(i,"%a+%-*_*%a+",function(x) i = x end)
					table.insert(t,i)
			else
					table.insert(t,i)
			end
		end
		local length = table.getn(t)         
		if (length==2) then
			boolean = x:delete(t[1],getConfType(t[1],t[2])[index+1][".name"],value)
		elseif (length==3) then 
			boolean = x:delete(t[1],getConfType(t[1],t[2])[index+1][".name"],t[3],value)
		end   
	else
		for i in string.gmatch(key, "(%w+_*-*%w+)%.-") do
			table.insert(t,i)
		end
		local length = table.getn(t)
		if (length==2) then
			boolean = x:delete(t[1],t[2],value)
		elseif (length==3) then
			boolean = x:delete(t[1],t[2],t[3],value)
		end	
	end

	if (boolean==true) then
		x:save(t[1])
		x:commit(t[1])
	end
end


-- function to process the response from Cloud server

function processResult(pbody)
	local newString = json.decode(pbody,1,nil)
	if (newString ~= nil) then

		debugLogging("debugInfo: Processing the succesful responses")
	for i=1, (table.getn(newString.cmds)) do
		if (newString.cmds[i].action=="localUpdateDeviceId") then
			local deviceId=newString.cmds[i].data
			fileWrite(deviceId)
			updateUrl() 
	        else
			findCommands(newString.cmds[i].action,newString.cmds[i].data)                                                         end
		end
	else
		sleep(configList["DEFAULT_SLEEP_TIME"])
		callGetCmd()
	end
	end


-- function to execute the command and return the config values as Lua tables

function executeCommand(rule)                
	local localTable = {}  
	local file = io.popen(rule)              
	for eachLine in file:lines() do             
		_, _, k, v = string.find(eachLine, "([^&=]+)=([^&=]+)")
		localTable[k]=v    
	end          
	file:close() 
	return localTable              
end              



--  Function to seperate json format of cloud response to each key/value pair 

function keyValue (t)
local key1,value1
for key,value in pairs(t) do
	if (key == "key") then
		key1 = value

	elseif (key == "value" ) then
		value1 = value
	end
	end	
return key1,value1 
end


--  Function to update/add given key/value value

function localUpdateConfig(jsonString)

        for key,value in pairs(jsonString) do
        	
        	-- calling Function to seperate json format of cloud response to each key/value pair

		key1,value1 = keyValue (value)
		-- update each key/value pair
		debugLogging("debugInfo: Updating each key value pair")
		updateKeyValue(key1,value1)
	end
end


-- Function to delete given config values

function localDeleteConfig(localTable)
	--local localTable = json.decode(jsonString,1,nil)
	for key1,value1 in pairs(localTable) do
	for key,value in pairs(value1) do
		-- deleting each key value pair
		debugLogging("debugInfo: Deleting each key value pair")
		deleteKeyValue(value)
	end
	end
end



-- Function to post the shell output value

function callPostShellOutput(instruction)
	local handle = io.popen(instruction)
	local reqbody = handle:read("*a")
	handle:close()
	local configTable = {}        
	firstKey = "instruction"
	configTable[firstKey] = instruction
	output = "output"
	configTable[output] = reqbody
	local configString = json.encode(configTable)
	local postUrl = url.."shellOutput"
	local respbody = {}
	debugLogging("debugInfo: call shell output API is invoked")
	local result, respcode, respheaders, respstatus = https.request{
	   	method = "POST",
		url=postUrl,
		mode = "client",                                                                     
                    protocol = "tlsv1",
		    cafile = "/root/server.pem",                            
                key = "/root/device.key",                                           
                certificate = "/root/client-cert.pem",                                                                  
                    verify = "peer",                                                                     
                    options = "all",
		source = ltn12.source.string(configString),
		body = configString,
		headers = {
		["content-type"] = "application/json",					                                
		["content-length"] = tostring(#configString)
		},
		sink = ltn12.sink.table(respbody)
		}  
	pbody = table.concat(respbody)
	if (respcode==200)   then
	processResult(pbody) 
	else
		if (respstatus ~= nil) then
		debugLogging("debugInfo: call shell output failed with status: " ..respstatus)
	end
	sleep(configList["DEFAULT_SLEEP_TIME"])
	callGetCmd()
	end
end


-- Function to send the config value of given key

	function callPutConfigs(data)
		local keyValue = {}
		for keyouter,valueouter in pairs(data) do
			for key,value in pairs(valueouter) do
					-- getting config value for each key value pair
		     		keyValue[value]=getConfig(value)
			end
	        end

		local reqbody = json.encode(keyValue)
		print (reqbody)
		local putUrl = url.."configs"
		local respbody = {}
		debugLogging("debugInfo: call put configs API is invoked ")
		local result, respcode, respheaders, respstatus = https.request{
			method = "PUT",
			url=putUrl,
			mode = "client",                                                                     
                    	protocol = "tlsv1",
			cafile = "/root/server.pem",                            
                key = "/root/device.key",                                           
                certificate = "/root/client-cert.pem",                                                                  
                    	verify = "peer",                                                                     
                    	options = "all",				
		        source = ltn12.source.string(reqbody),
		        body = reqbody,
	        	headers = {
	                   ["content-type"] = "application/json",	
	                   ["content-length"] = tostring(#reqbody)
						},  
				sink = ltn12.sink.table(respbody) } 
				pbody = table.concat(respbody)
				if (respcode==200) then
				processResult(pbody) 
				else
					if (respstatus ~= nil) then
					debugLogging("debugInfo: call put configs API failed with status: " ..respstatus)
				end
				sleep(configList["DEFAULT_SLEEP_TIME"])
				callGetCmd()
				end
	end


-- Function to post the configuration of Router

function callPostConfigs() 
	-- calling function to get all the config values as Lua tables
	local configTable = executeCommand('uci show')  
	local reqbody = json.encode( configTable )    
	local postUrl = url.."configs"  
	local respbody = {}   
	debugLogging("debugInfo: call post configs is invoked")      
	local result, respcode, respheaders, respstatus = https.request{ 
		method = "POST",       
		url=postUrl,
		mode = "client",                                                                     
                    protocol = "tlsv1",
		    cafile = "/root/server.pem",                            
                key = "/root/device.key",                                           
                certificate = "/root/client-cert.pem",                                                                  
                    verify = "peer",                                                                     
		options = "all",           
		source = ltn12.source.string(reqbody),   
		body = reqbody,
		headers = {            
		["content-type"] = "application/json",     
		["content-length"] = tostring(#reqbody)                
		},   
		sink = ltn12.sink.table(respbody)        
		}  
	pbody = table.concat(respbody)  
	if (respcode==200)   then
            processResult(pbody) 
            else
            	if (respstatus ~= nil) then
            	debugLogging("debugInfo: call Post Configs failed with status: " ..respstatus)
            	end
            sleep(configList["DEFAULT_SLEEP_TIME"])
            callGetCmd()
            end 
end

-- Function to send alert codes to cloud server during reboot

function sendAlert() 
	local alertTable = {}
	alertTable["alertCode"] = configList["REBOOT_ALERT_CODE"]
	alertTable["alertMessage"] = configList["REBOOT_ALERT_MESSAGE"]
	local reqbody = json.encode( alertTable )    
	local alertUrl = url.."alerts"  
	local respbody = {}    
	debugLogging("debugInfo: sendAlert API is invoked")      
	local result, respcode, respheaders, respstatus = https.request{ 
		method = "POST",       
		url=alertUrl,
		mode = "client",                                                                     
                    protocol = "tlsv1",
		    cafile = "/root/server.pem",                            
                key = "/root/device.key",                                           
                certificate = "/root/client-cert.pem",                                                                  
                    verify = "peer",                                                                     
		options = "all",           
		source = ltn12.source.string(reqbody),   
		body = reqbody,
		headers = {            
		["content-type"] = "application/json",     
		["content-length"] = tostring(#reqbody)                
		},   
		sink = ltn12.sink.table(respbody)        
		}  
	pbody = table.concat(respbody)  
	
end






-- Function to make the device sleep for n seconds
function sleep(n)
	debugLogging("debugInfo: Device is going to sleep for second: "..n) 
	os.execute("sleep " .. tonumber(n))
end


-- Function to update the URL whenever device id is changed by cloud
function updateUrl()		      
	local deviceCheck = fileRead()
	debugLogging("debugInfo: Url is updated with device Id") 
	url=string.gsub(url, "/%d/", "/"..deviceCheck.."/")
end


-- Function to reboot the device
function localReboot()
	debugLogging("debugInfo: Reboot is invoked") 
	os.execute("reboot")
end


-- Function to download script file

function downloadScript(fileName)
	local downloadUrl = configList["SERVER_PROTO"].."://"..configList["SERVER_IP"].."/download/"..fileName	
	
    debugLogging("debugInfo: downloadScript API is invoked") 
	local respbody = {}                                                      
            local result, respcode, respheaders, respstatus = https.request{                              
                    url=downloadUrl,                                                
                    mode = "client",                            
                    protocol = "tlsv1", 
		    cafile = "/root/server.pem",                            
                key = "/root/device.key",                                           
                certificate = "/root/client-cert.pem",                        
		verify = "peer",                                                   
                    options = "all",   
		sink = ltn12.sink.table(respbody)                                             
                    }     
     debugLogging("debugInfo: Download script failed with status: " ..respstatus)
	pbody = table.concat(respbody)
 	file = io.open(configList["DEV_FILE_PATH"].."script.sh", "w")
	file:write(pbody)
	file:close()
	debugLogging("debugInfo: Downloaded Script is going to be executed") 
	os.execute('sh '..configList["DEV_FILE_PATH"]..'script.sh >'..configList["DEV_FILE_PATH"]..configList["DEV_DEBUG_OP_FILE"])                                                                   
end


-- Function to upload file REST API call

function callPostUploadFile()

	local postFileUrl = url.."uploadFile"
	local command = 'curl -i -k -H "Content-Type: multipart/form-data" -X POST -F data=@'..configList["DEV_FILE_PATH"]..configList["DEV_DEBUG_OP_FILE"]..' \"'..postFileUrl..'\" '..'--cert /root/client-cert.pem --cert-type PEM --key /root/device.key --cacert /root/server.pem'
	debugLogging("debugInfo: uploadFile API is invoked") 
	os.execute(command)
    sleep(5)
    callGetCmd()
end


-- Function to call the next command API call

function  callGetCmd()
	local getCmdUrl = url.."cmd"
	local respbody = {}    
	debugLogging("debugInfo: Call get command API is invoked")                             
            local result, respcode, respheaders, respstatus = https.request{
                    url=getCmdUrl,                            
                    mode = "client",                            
                    protocol = "tlsv1", 
		    cafile = "/root/server.pem",                            
                key = "/root/device.key",                                           
                certificate = "/root/client-cert.pem",                        
			verify = "peer",                            
                    options = "all",                            
                    sink = ltn12.sink.table(respbody)           
                    }                                           
            pbody = table.concat(respbody)  
            if (respcode==200) then
            	processResult(pbody) 
            else
            	if (respstatus ~= nil) then
            		debugLogging("debugInfo: call get command failed with status: " ..respstatus)
            	end
            sleep(configList["DEFAULT_SLEEP_TIME"])
            callGetCmd()
            end
end


-- function to find the actions to be executed from the Cloud 

function findCommands(action,data)
	if (action=="callPostConfigs") then
		callPostConfigs()
	elseif (action=="callGetCmd") then
		callGetCmd()
	elseif (action =="callPostShellOutput") then
		callPostShellOutput(data)
	elseif (action == "localSleep") then
		sleep(data/1000)
	elseif (action == "localUpdateConfig") then
		localUpdateConfig(data)
	elseif (action == "localDeleteConfig") then
		localDeleteConfig(data)
	elseif (action == "callPutConfigs") then
		callPutConfigs(data)
	elseif (action == "localReboot") then
		localReboot()
	elseif (action == "downloadScript") then
		downloadScript(data)			
	elseif (action == "callPostUploadFile") then
		callPostUploadFile()
	else
		sleep(configList["DEFAULT_SLEEP_TIME"])		
		callGetCmd()
	end
end



-- Function to register the Device

function registerDevice()
	local registerUrl = url.."cmd?hardwareType="..configList["DEV_HW_TYPE"].."&softwareType="..configList["DEV_SW_TYPE"]  
	debugLogging("debugInfo: Sending Register request with Software and Hardware type ")
	local respbody = {}                                                                                                     
            local result, respcode, respheaders, respstatus = https.request{
                    url=registerUrl,
			mode = "client",       
		protocol = "tlsv1",			                                     
			cafile = "/root/server.pem",
		key = "/root/device.key",
		certificate = "/root/client-cert.pem",	
			verify = "peer",
			options = "all",  
                    sink = ltn12.sink.table(respbody)                                        
                    }                                                       
            pbody = table.concat(respbody)  
            if (respcode==200) then
            	processResult(pbody)
            else
            	if (respstatus ~= nil) then
            	debugLogging("debugInfo: registration failed with status: " ..respstatus)
            end
            	sleep(configList["DEFAULT_SLEEP_TIME"])
            	registerDevice()
            end
end


-- Check if the file exists in the given path

function fileCheck(file_name)
	local file_found=io.open(file_name, "r")     
  	return file_found
end

                                              

-- reads data from config file and returns the value in table. Returns nil if the file does not exist.     
function readConfig(file)                    
  if (fileCheck(file) == nil) then
    debugLogging("debugInfo: There is no config file. Please add it")
    return {} end       
  lines = {}                                        
  for line in io.lines(file) do                     
      if (line ~= nil and line ~= "") then   
      			line = string.gsub(line," ","")           
                pos = string.find(line,"=")             
                firstpart = string.sub(line,1,pos-1)    
                secondpart = string.sub(line,pos+1)           
                poscomment = string.find(secondpart,";")      
                if (poscomment == nil)   then                 
                        lines[firstpart] = secondpart         
                else                                          
                        secondpart = string.sub(secondpart,1,poscomment-1)
                        lines[firstpart] = secondpart
                end
      end                   
  end                       
  return lines                
end                           
      

-- starting main file execution


-- loading the configuration file  

local configFile = 'client.ini'                   
configList = readConfig(configFile)

-- forming URL
url = configList["SERVER_PROTO"].."://"..configList["SERVER_IP"].."/device/0/"

	
-- Checking for device id file. If it does not present, then it is first registration else device has rebooted and it contacts 
-- cloud with existing id 


if (fileCheck(configList["DEV_FILE_PATH"]..configList["DEV_ID_FILE"])==nil)  then
	debugLogging("debugInfo: Register device is invoked")
	registerDevice()		
else
	updateUrl()
	debugLogging("debugInfo: sending Reboot Info")
	sendAlert()
	debugLogging("debugInfo: Calling Next command Api")
	sleep(configList["DEFAULT_SLEEP_TIME"])	
	callGetCmd()				
end


