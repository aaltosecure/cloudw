#!/bin/sh 

#######################################
# Configurations for the gateway-router

# Kill existing daemons
killall -q lua
killall -q stunnel



# Start tunnel entry point
stunnel &

# Redirect all HTTPS traffic to the cloud server to the tunnel entry point
iptables -t nat -I PREROUTING --dest 130.233.193.174 -p tcp --dport 443 -j DNAT --to-dest 192.168.1.1:8080

# Start lighttpd, which redirects incoming HTTP to the cloud service's IP
# lighttpd -f /jffs/etc/lighttpd.conf &

# Start the client that fetches configs
lua client.lua &
