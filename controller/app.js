var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var session = require('express-session');
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var util = require('util');
var busboy = require('connect-busboy');
var fs = require('fs');


var routes = require('./routes/index');
var device = require('./routes/device');
var devices = require('./routes/devices');
var registerDevice = require('./routes/registerDevice');
var setup = require('./routes/setup.js');
var ajax = require('./routes/ajax.js');
var other = require('./routes/other.js');
var auth = require('./routes/auth.js');
var download = require('./routes/download.js');
var logger = require("./utils/logger.js");
var UserDeviceMapping = require('./model/userDeviceMapping.js');

var DeviceConnection = require('./model/deviceConnection');
var Device = require('./model/device');
var KeyMappings = require('./model/keyMappings.js');
var KeyCache = require('./model/keyCache.js');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


// uncomment after placing favicon in /public
// app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(logger('dev'));
app.use(bodyParser.json());
// Limits need to be larger than the default values
app.use(bodyParser.urlencoded({ extended: false, limit: '200kb', parameterLimit: 2000 }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(busboy());

/* User authentication with passport */
require('./utils/passport.js')(passport); // pass passport for configuration
var sessionOpts = {
	secret: 'verysecretsecretindeed',
	resave: false,
	saveUninitialized: true
};
app.use(session(sessionOpts));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(flash());

//Create folders for log files
fs.mkdir('logs', 0777, function(err) {});//mask: 0777
//Create folders for uploaded files from devices
fs.mkdir('deviceFiles', 0600, function(err) {});//mask: 0777
//Create folders for files to be downloaded by devices
fs.mkdir('public/filesForDownload', 0777, function(err) {});//mask: 0777


/* Indentify if connection comes through tunnel. 
 * Add device fingerprint to reg.deviceFingerprint and session 
 * if it came throgh one.  
*/
app.use(function(req, res, next) {
	/* Is this the initial connection. I.e. is session.deviceFingerprint set? */
	if(typeof req.session.deviceFingerprint == "undefined") {
		DeviceConnection.findOne({where: {port: req.socket.remotePort}}, function(err, devCon) {
			if(err) {
				logger.writeLog('error', null, null, err.toString());
				res.send("Error");
				return;
			} else if(devCon) {
				// Connection through the tunnel
				logger.writeLog('debug', null, null,
					'remotePort: ' + req.socket.remotePort +
					' mapped to device fingerprint: ' + devCon.key);
				req.deviceFingerprint = devCon.key;
			} else {
				// Connection not through the tunnel
				logger.writeLog('debug', null, null,
					'No Device Connection found for remotePort: ' + req.socket.remotePort);
				req.deviceFingerprint = false;
			}

			res.locals.deviceFingerprint = req.deviceFingerprint; // Exposing this to JADE
			req.session.deviceFingerprint = req.deviceFingerprint;
			req.session.save();
			next();
		});
	} else {
		res.locals.deviceFingerprint = req.session.deviceFingerprint; // Exposing this to JADE
		req.deviceFingerprint = req.session.deviceFingerprint;
		next();
	}
});

/* Check if the connection came through a device that has not been paired
 * with a user.
 * This action is performed for all requests not made to /registerDevice
 */
app.use("(?!/registerDevice)", function(req, res, next) {
	// Did the connection come through a tunnel?
	if(req.deviceFingerprint) {
		// Has the device been paired with a user
		Device.findOne({where: {key: req.deviceFingerprint}}, function(err, device) {
			if(err !== null) {
				logger.writeLog('error', null, null, err.toString());
				res.send("Error");
				return;
			} else if (device === null) {
					// Connection came through a device that has not been paired
					logger.writeLog('debug', null, null,
						'Connection came through a device that has not been paired');
					res.redirect('/registerDevice/');
			} else{
				UserDeviceMapping.findOne({where: {deviceId: device.id, deviceAccessLevel: 'OWNERSHIP'}}, function (err, mapping) {
					if(mapping) {
						// Connection came through a device that has been paired
						logger.writeLog('debug', null, null,
							'Connection came through a device that has been paired');
						next();
					}
					else {
						// Connection came through a device that has not been paired
						logger.writeLog('debug', null, null,
							'Connection came through a device that has not been paired');
						res.redirect('/registerDevice/');
					}
				});
			}
		});
	} else 
		// Connection did not come through a tunnel
		next();
});


/* Make session visible to JADE */
app.use(function(req, res, next) {
	res.locals.session = req.session;
	next();
});



/* Basic routes */
app.use('/', routes);
app.use('/device', device);
app.use('/devices', devices);
app.use('/registerDevice', registerDevice);
app.use('/setup', setup);
app.use('/ajax', ajax);
app.use('/other', other);
app.use('/auth', auth);
app.use('/download', download);


/* Login and logouts */
app.post('/login', passport.authenticate('local-login', {
	successRedirect : '/',
	failureRedirect : '/login',
	failureFlash: true
}));
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

/**
 * REST-API to download files from public folder
 */
app.get('/download/:filename', function(req, res) {
	logger.writeLog('info', null, null, 'Called:' + req.originalUrl.toString());
	var file = 'public/filesForDownload/' + req.params.filename;
	res.download(file);
});


/****************************************/
/* Debug and error handling stuff below */





// Can be used to check if a gateway switch is trusted 
app.get('/whoami', function(req, res) {
  // AUTHORIZED 
  if(req.client.authorized){
 	var peerCert = req.connection.getPeerCertificate();
    var subject = req.connection
      .getPeerCertificate().subject;
     
    // Render the authorized template, providing
    // the user information found on the certificate
    res.send({msg: 'authorized', peerCert: peerCert}); 
  
  // NOT AUTHORIZED
  } else {
    res.send({msg: 'NOT AUTHORIZED'});  
  }
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// Initial cache building
KeyCache.refresh();

// Global cache refresh interval
app.interval = setInterval(function() {
  KeyCache.refresh();
}, 60000);

// Debug

app.interval2 = setInterval(function() {
  console.log(KeyCache.data);
}, 1000);

module.exports = app;
