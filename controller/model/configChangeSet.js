/**
 * configChangeSet represents a change request
 * created by a user for a specific device
 */
var schema = require('./schema.js');
var User = require('./user.js');
var Device = require('./device.js');

var ConfigChangeSet = schema.define('ConfigChangeSet', {
    key:        {type: String},
    value:      {type: String}
});
ConfigChangeSet.belongsTo(User, {as: 'user', foreignKey: 'userId'});
ConfigChangeSet.belongsTo(Device, {as: 'device', foreignKey: 'deviceId'});

module.exports = ConfigChangeSet;
