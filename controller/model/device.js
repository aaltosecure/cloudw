/**
 * device represents a router
 * along with associated parameters and flags
 */
var schema = require('./schema.js');
var DeviceAlertLog = require('./deviceAlertLog.js');
var ShellCommandLog = require('./shellCommandLog.js');
var DeviceType = require('./deviceType.js');
var UserDeviceMapping = require('./userDeviceMapping.js');
var ConfigChangeSet = require('./configChangeSet.js');

var Device = schema.define('Device', {
	key:					{type: String, index: true},
	allowOffband:			{type: Boolean, default: false},
	state:					{type: String, default: 'NORMAL'},
	lastLockTime:			{type: Number},
	isDebugInfoRequested:	{type: Boolean, default: false}
});

Device.hasMany(DeviceAlertLog, {as: 'deviceAlertLogs',  foreignKey: 'deviceId'});
//Device.hasMany(ShellCommandLog, {as: 'shellCommandLogs',  foreignKey: 'deviceId'});
Device.hasMany(UserDeviceMapping, {as: 'userDeviceMappings',  foreignKey: 'deviceId'});
Device.hasMany(ConfigChangeSet, {as: 'configChangeSets',  foreignKey: 'deviceId'});

Device.belongsTo(DeviceType, {as: 'deviceType', foreignKey: 'deviceTypeId'});

module.exports = Device;
