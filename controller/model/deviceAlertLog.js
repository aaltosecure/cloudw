/**
 * deviceAlertLog represents an alert
 * sent proactively by a device to the server
 */
var schema = require('./schema.js');
var Device = require('./device.js');

var DeviceAlertLog = schema.define('DeviceAlertLog', {
    alertCode:      {type: String},
    msg:            {type: String},
    timestamp:      {type: Number, default: Date.now}
});
DeviceAlertLog.belongsTo(Device, {as: 'device', foreignKey: 'deviceId'});

module.exports = DeviceAlertLog;