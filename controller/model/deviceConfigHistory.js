/**
 * deviceConfigHistory holds the configuration snapshots
 * for a specific device
 * taken whenever the configuration is changed
 */
var schema = require('./schema.js');

var DeviceConfigHistory = schema.define('DeviceConfigHistory', {
    deviceId:       {type: Number},
    snapshot:		{type: String},
    timestamp:      {type: Number}
});

module.exports = DeviceConfigHistory;
