/**
 * deviceConnection hold the port-fingerprint mappings
 * for connections
 */
var schema = require('./schema.js');

var DeviceConnection = schema.define('DeviceConnection', {
	key:	{type: String},
	port:	{type: Number, index: true},
	timestamp: {type: Number, default: Date.now} 	
});

module.exports = DeviceConnection;

