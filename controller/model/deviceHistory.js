/**
 * deviceHistory logs any change
 * in the Device table
 */
var schema = require('./schema.js');

var DeviceHistory = schema.define('DeviceHistory', {
    deviceId:       {type: Number},
    key:			{type: String},
    allowOffband:	{type: Boolean},
    deviceTypeId:   {type: Number},
    state:		    {type: String},
    lastLockTime:	{type: Number},
    timestamp:      {type: Number}
});

module.exports = DeviceHistory;
