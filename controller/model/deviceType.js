/**
 * deviceType represents the hardware types and software types
 * identified by the server
 */
var schema = require('./schema.js');
var Device = require('./device.js');

var DeviceType = schema.define('DeviceType', {
    hardwareType:	{type: String},
    softwareType:	{type: String}
});

module.exports = DeviceType;
