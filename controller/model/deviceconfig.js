/**
 * deviceConfig represents configuration key-value pairs
 * for a specific device
 */
var schema = require('./schema.js');
var Device = require('./device.js');

var CurrentDeviceConfig = schema.define('CurrentDeviceConfig', {
	userId: {type: String},
	key:	{type: String},
	value:	{type: String}

});
CurrentDeviceConfig.belongsTo(Device, {as: 'device', foreignKey: 'deviceId'});

module.exports = CurrentDeviceConfig;
