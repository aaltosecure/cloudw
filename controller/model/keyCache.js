var KeyMappings = require('./keyMappings.js');
var DeviceType = require('./deviceType.js');

KeyCache = { }

KeyCache.refresh = function() {
  KeyMappings.all({ }, function(err, mappings) {
    KeyCache.data = mappings;
  });

  DeviceType.all({ }, function(err, types) {
    KeyCache.types = types;
  });


}

/*
if (!("interval" in KeyCache)) {
  KeyCache.refresh();
  KeyCache.interval =  setInterval(KeyCache.refresh(), 1000);
}
*/

module.exports = KeyCache;
