/**
 * keyMappings map UI input fields to keys
 * based on the type of device
 */
var schema = require('./schema.js');
var DeviceType = require('./deviceType.js');

var KeyMappings = schema.define('KeyMappings', {
    inputName:	{type: String},
    key: {type: String}
});

KeyMappings.belongsTo(DeviceType, {as: 'deviceType', foreignKey: 'deviceTypeId'});

module.exports = KeyMappings;

