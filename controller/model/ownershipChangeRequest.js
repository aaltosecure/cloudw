/**
 * ownershipChangeRequest represents a change in ownership
 * initiated by the old owner of the device
 */
var schema = require('./schema.js');

var OwnershipChangeRequest = schema.define('OwnershipChangeRequest', {
    oldOwner_Id:        {type: Number},
    newOwner_Email:     {type: String},
    old_DeviceId:       {type: Number},
    new_DeviceId:       {type: Number},
    isInfoShared:	    {type: Boolean},
    isHandled:	        {type: Boolean}
});

module.exports = OwnershipChangeRequest;

