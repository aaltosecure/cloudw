/**
 * schema represents the database connected to
 */
var Schema = require('jugglingdb').Schema;

var schema = new Schema('mysql', {
	database: 'router_cloud',
	username: 'root',
	password: 'jeejee'
});

/* //For SQLite3, use this
var schema = new Schema('sqlite3', {
 	database: 'data/testdb05.sqlite'
});*/

module.exports = schema;
