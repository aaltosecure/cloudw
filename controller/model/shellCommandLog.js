/**
 * shellCommandLog represents the shell commands
 * requested by users
 * to be run on specific devices
 */
var schema = require('./schema.js');
var User = require('./user.js');
var Device = require('./device.js');

var ShellCommandLog = schema.define('ShellCommandLog', {
    shellCommand:             {type: String},
    output:                   {type: String},
    cmdRequestTimestamp:      {type: Number}
});
ShellCommandLog.belongsTo(User, {as: 'user', foreignKey: 'userId'});
ShellCommandLog.belongsTo(Device, {as: 'device', foreignKey: 'deviceId'});

module.exports = ShellCommandLog;
