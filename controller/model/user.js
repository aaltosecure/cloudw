/**
 * user represents registered users in the system
 * with email as the main identifier
 */
var bcrypt = require('bcrypt-nodejs');
var schema = require('./schema.js');
var ShellCommandLog = require('./shellCommandLog.js');
var UserDeviceMapping = require('./userDeviceMapping.js');
var ConfigChangeSet = require('./configChangeSet.js');

var User = schema.define('User', {
	email:      {type: String},
	password:   {type: String}
});

User.hasMany(ShellCommandLog, {as: 'shellCommandLogs',  foreignKey: 'userId'});
User.hasMany(UserDeviceMapping, {as: 'userDeviceMappings',  foreignKey: 'userId'});
User.hasMany(ConfigChangeSet, {as: 'configChangeSets',  foreignKey: 'userId'});
  
User.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

User.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = User;
