/**
 * @type {schema|exports|module.exports}
 */
var schema = require('./schema.js');
var User = require('./user.js');
var Device = require('./device.js');

var UserDeviceMapping = schema.define('UserDeviceMapping', {
    deviceAccessLevel:     {type: String},
    accessExpiryTime:      {type: Number}
});
UserDeviceMapping.belongsTo(User, {as: 'user', foreignKey: 'userId'});
UserDeviceMapping.belongsTo(Device, {as: 'device', foreignKey: 'deviceId'});

module.exports = UserDeviceMapping;