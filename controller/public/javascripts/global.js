var globalCounter = 50;
var globalTimer = 3000;
var seconds = null;
var intervalId;

var Devices = { }

// DOM Ready =============================================================
$(document).ready(function() {
	/**
	 * List the devices belonging to the user.
	 */
	populateDevices();
	/**
	 * Enable clicking between tabs in the UI.
	 */
	setTabs();
	if(deviceFingerprint == "false")
		$('#allowOffband').attr("disabled", "disabled");

	/**
	 * User selected another device from the list.
	 */
	$('#deviceList').on("change", function() {
		// Selected device.id
		deviceId = $('#deviceList').val();

		// User selected the "Select device"-option. Empty the table.
		if(!deviceId) {
			$(".hideIfNoDeviceSelected").css("visibility", "hidden");
			$('#configList table tbody').html('');
			$('#logs table tbody').html('');
		} else {
			checkAllowOffband(deviceId);
			$(".hideIfNoDeviceSelected").css("visibility", "visible");
			/**
			 * Update the user interface with the information regarding the device the user selected.
			 */
			//populateConfigs($('#deviceList').val());
			//populateLogs($('#deviceList').val());
			//populateHistory($('#deviceList').val());

			$("input[name=devId]").val(deviceId);
			getVersions(deviceId);
			updateNormal();
		}
	});

	$('#allowOffband').on("change", function() {
		allowOffband($('#deviceList').val());
	});

	/**
	 * Configuration list table in the Expert Tab.
	 */
	$('#configList table tbody').on('click', 'td a.deleteconfig', deleteConfig);
	$('#configList table tbody').on('click', 'td a.editconfig', editConfig);

        $
});

/**
 * List the devices belonging to the logged in user.
 */
function populateDevices() {
    $.getJSON( '/devices/list', function( data ) {
                Devices = data;
		$.each(data, function() {
			if(!this.allowOffband && this.key != deviceFingerprint) {
				$('#deviceList')
					.append($("<option></option>")
					.attr("value",this.id)
					.attr("disabled", "disabled")
					.text(this.key));
			} else {

				$('#deviceList')
					.append($("<option></option>")
					.attr("value",this.id)
					.text(this.key));
			}
		});
	});
}

/**
 * Show the different dates when the user changed the configurations so that he can revert them back.
 * @param deviceId
 */
function populateHistory(deviceId) {
    $.getJSON('/other/history/'+deviceId, function (data) {
        $.each(data, function () {
            $('#historyList')
                .append($("<option></option>")
                    .attr("value", this.timestamp)
                    .text(new Date(this.timestamp).toString()));
        });
    })
}

/**
 * Call the server to restore the previous configuration with the timestamp equal to historyList select option.
 */
function restoreConfig() {
	deviceId = $('#deviceList').val();
	historyList = $('#historyList').val();
	$.post('/other/restoreConfig', {
		devId: deviceId,
		historyList: historyList
	},function (response) {
		if(response.error == '') {
			$("#histresult").text('The previous configuration was successfully restored');
		}
		else {
			$("#histresult").text(response.error);
		}
	});
}

/**
 * Show the configuration in the Expert Tab.
 * @param deviceId
 */
function populateConfigs(deviceId) {
	// Populate configs
    var tableContent = '';

	$("#addConfig").attr("action", "/setup/" + deviceId + "/addConfig");

    $.getJSON( '/devices/listConfigs/' + deviceId, function( data ) {
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td id=' + this.id + ' >' + this.key + '</td>';
            tableContent += '<td>' + this.value + '</td>';
	    			tableContent += '<td><a href="#" class="editconfig" rel="['
				+ deviceId + ',' + this.id + ']">edit</a></td>';
            tableContent += '<td><a href="#" class="deleteconfig" rel="['
				+ deviceId + ',' + this.id + ']">del</a></td>';
            tableContent += '</tr>';
        });

        // Inject the whole content string into our existing HTML table
        $('#configList table tbody').html(tableContent);
	});
}

/**
 * Show the logs in the OTHER/LOGS TAB.
 * @param deviceId
 */
function populateLogs(deviceId) {
	var tableContent = '';
	$.getJSON('/other/logs/' + deviceId, function (data) {
		$.each(data, function () {
			tableContent += '<tr>';
			tableContent += '<td>' + this.deviceId  + '</td>';
			tableContent += '<td>' + this.alertCode + '</td>';
			tableContent += '<td>' + this.msg  + '</td>';
			tableContent += '<td>' + new Date(this.timestamp).toString()  + '</td>';
			tableContent += '</tr>';
		});
		$('#logs table tbody').html(tableContent);
	});
}

/**
 * Edit configuration in the EXPERT TAB.
 * @param event
 * @returns {boolean}
 */

function editConfig(event) {

    event.preventDefault();
	var newValue = prompt("Please enter the new value", "");

    // Check and make sure the user confirmed
    if (newValue != null) {
		deviceId = JSON.parse($(this).attr('rel'))[0];
		configId= JSON.parse($(this).attr('rel'))[1];
		key = document.getElementById(configId).innerHTML;

		$.post('/devices/editConfig',{
			newValue: newValue,
			devId: deviceId,
			key: key
		}, function( response ) {
			if (response.error === '') {}
			else {
				alert('Error: ' + response.error);
			}
			var identifier = 'editConfig';
			getState(deviceId, globalCounter, identifier);
        });
	}
    else {
        return false;
    }
}

/**
 * Delete configuration in the EXPERT TAB.
 */
function deleteConfig(event) {

    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this config?');

    // Check and make sure the user confirmed
    if (confirmation === true) {
		deviceId = JSON.parse($(this).attr('rel'))[0];
		configId= JSON.parse($(this).attr('rel'))[1];
		key = document.getElementById(configId).innerHTML;

		$.post('/devices/deleteConfig', {
			devId: deviceId,
			key: key
		},function( response ) {
			if (response.error === '') {}
			else {
				alert('Error: ' + response.error);
			}
			populateConfigs($('#deviceList').val());
		});
    }
    else {
        return false;
    }
}

/**
 * If allowOffBand is checked, the user can manage the device even if it not connected through the device.
 * @param deviceId
 */
function checkAllowOffband(deviceId) {
        /*
	$.getJSON( '/devices/' + deviceId, function( data ) {
		$('#allowOffband').prop('checked', data.allowOffband);
	});
        */

        $.each(Devices, function(device) {
		if(device.id == deviceId) {
			$('#allowOffband').prop('checked', device.allowOffband);
                }
        });
}

// Alter offband
function allowOffband(deviceId) {
	var value = $('#allowOffband').is(":checked");
	var reqdata = {allowOffband: value};

	$.ajax({
		type: 'POST',
		url: '/devices/'+ deviceId,
		data: reqdata
	}).done(function( response ) {
		if (response.msg === '') {}
		else {
			alert('Error: ' + response.msg);
		}

		//populateConfigs($('#deviceList').val());
	});
}

/**
 * List the names of the debugging files regarding the device the user selected.
 */
function requestDebugInfo(){
	deviceId = $('#deviceList').val();
	$('#filenamesTable table tbody').html('');
	$("#filenames").text('Waiting for the debugging information');
	$.ajax({
		type: 'GET',
		url: '/download/debug/request/' + deviceId
	}).done(function (response) {
		getDebugFiles(deviceId, globalCounter);
	});
}

/**
 * Repeatedly ask for the debugging files until they are available from the device.
 * @param deviceId
 * @param counter
 */
function getDebugFiles(deviceId, counter) {
	$.ajax({
		type: 'GET',
		url: '/download/debug/filenames/' + deviceId
	}).done(function (response) {
		if (response.filename == '') {
			setTimeout(function () {
				if (counter > 0) {
					counter--;
					getDebugFiles(deviceId, counter);
				}
			}, globalTimer);
		}
		else {
			var filenamesTable = '';
			$("#filenames").text('The debugging information was successfully received');
			for(var r in response) {
				filenamesTable += '<tr>';
				filenamesTable += '<td><a href="/download/debug/files/' + deviceId + '/' + response[r].filename + '" class="downloadFiles" rel="[' + response[r].filename + ']">' + response[r].filename + '</a></td>';
				filenamesTable += '</tr>';
			}
			$('#filenamesTable table tbody').html(filenamesTable);
		}
	});
}

/**
 * Get router and os version and show it in the user interface.
 * @param deviceId
 */
function getVersions (deviceId) {
	$.ajax({
		type: 'GET',
		url: '/ajax/versions/' + deviceId
	}).done(function (response) {
		$("#routerversion").text('Router version is ' + response.routerversion);
		$("#osversion").text('Os version is ' + response.osversion);
	});
}

/**
 * Send to the server the user input in the EASY TAB. Wait until the changes are applied in the device and update UI.
 */
function easy() {
	wirelessname = document.getElementById('easywlname').value;
	wpawirelesspw = document.getElementById('easywlpw').value;
	disableInput();
	$("#easyresult").text('Waiting to apply the changes to the device');
	$.post('/setup/easy', {
		devId: deviceId,
		wirelessname: wirelessname,
		wpawirelesspw: wpawirelesspw
	},function (response) {
		if(response.error == '') {
			var identifier = 'easy';
			getState(deviceId, globalCounter, identifier);
		}
		else {
			$("#easyresult").text(response.error);
		}
	});
}

/**
 * Repeatedly ask for the state of the device. If the changes are applied in the device, then update UI.
 * @param deviceId
 * @param counter
 * @param identifier
 */
function getState(deviceId, counter, identifier) {
	$.ajax({
		type: 'GET',
		url: '/setup/state/check/' + deviceId
	}).done(function (response) {
		if(response.retry == true) {
			setTimeout(function(){
				if(counter > 0) {
					counter--;
					getState(deviceId, counter, identifier);
				}
				else {
					enableInput();
				}
			}, globalTimer);
		}
		else {
			switch (identifier) {
				case 'easy': {
					updateNormal();
					$("#easyresult").text('The changes were successfully applied');
				} break;
				case 'normal': {
					updateNormal();
					$("#normalresult").text('The changes were successfully applied');
				} break;
				case 'editConfig': populateConfigs($('#deviceList').val()); break;
				default: break;
			}
		}
	});

}

/**
 * Send to the server the user input in the NORMAL TAB. Wait until the changes are applied in the device and update UI.
 */
function normalFunc(){
	deviceId = $('#deviceList').val();
	routername = document.getElementById('routername').value;
	localip = document.getElementById('localip').value;
	subnetmask = document.getElementById('subnetmask').value;
	defaultgw = document.getElementById('defaultgw').value;
	localdns = document.getElementById('localdns').value;
	wirelessname = document.getElementById('normalwlname').value;
	wirelesspw = document.getElementById('normalwlpw').value;

	var ap = "";
	var apRadio = $("input[type='radio'][name='ap']:checked");
	if (apRadio.length > 0) {
		ap = apRadio.val();
	}
	else {
		ap = 'yes';
	}


	var ssidbcast = "";
	var ssidbcastRadio = $("input[type='radio'][name='ssidbcast']:checked");
	if (ssidbcastRadio.length > 0) {
		ssidbcast = ssidbcastRadio.val();
	}
	else {
		ap = 'yes';
	}
	$("#result").text('Waiting to apply the changes to the device');
	disableInput();
	$.post('/setup/normal', {
		devId: deviceId,
		routername: routername,
		localip: localip,
		subnetmask: subnetmask,
		defaultgw: defaultgw,
		localdns: localdns,
		wirelessname: wirelessname,
		wirelesspw: wirelesspw,
		ap: ap,
		ssidbcast: ssidbcast
	}, function (response) {
		if(response.error == '') {
			var identifier = 'normal';
			getState(deviceId, globalCounter, identifier);
		}
		else {
			$("#result").text(response.error);
		}
	});
}

/**
 * Update the UI in the NORMAL TAB.
 */
function updateNormal() {
	deviceId = $('#deviceList').val();
	$.ajax({
		type: 'GET',
		url: '/ajax/normal/' + deviceId
	}).done(function (response) {
		$("input[id=routername]").val(response.routername);
		$("input[id=localip]").val(response.localip);
		$("input[id=subnetmask]").val(response.subnetmask);
		$("input[id=defaultgw]").val(response.defaultgw);
		$("input[id=localdns]").val(response.localdns);
		$("input[id=normalwlname]").val(response.wirelessname);
		$("input[id=normalwlpw]").val(response.wirelesspw);
		$("input[id=wlpasswtype]").val(response.wlpasswtype);
		$("input[id=wlnetmode]").val(response.wlnetmode);
		$("input[id=ap]").val(response.ap);
		$("input[id=ssidbcast]").val(response.ssidbcast);
                $("input[id=easywlname]").val(response.wirelessname);
                $("input[id=easywlpw]").val(response.wirelesspw);
		enableInput();
	})
}

/**
 * Update the UI in the ADVANCED/NETWORK TAB
 */
function updateNetworkView() {
	deviceId = $('#deviceList').val();
	$.ajax({
		type: 'GET',
		url: '/ajax/advanced/setup/' + deviceId
	}).done(function (response) {
		$("input[name=routername]").val(response.routername);
		$("input[name=localip]").val(response.localip);
		$("input[name=subnetmask]").val(response.subnetmask);
		$("input[name=defaultgw]").val(response.defaultgw);
		$("input[name=localdns]").val(response.localdns);
		$("input[name=hostname]").val(response.hostname);
		$("input[name=domainname]").val(response.domainname);
		$("input[name=mtu]").val(response.mtu);
		$("input[name=dhcpip]").val(response.dhcpip);
		$("input[name=maxdhcp]").val(response.maxdhcp);
		$("input[name=leasedhcp]").val(response.leasedhcp);
		$("input[name=sdnsone]").val(response.sdnsone);
		$("input[name=sdnstwo]").val(response.sdnstwo);
		$("input[name=sdnsthree]").val(response.sdnsthree);
	});
}

/**
 * Send the shell command the user inserted, to the device and wait for the output.
 */
function command() {
	deviceId = $('#deviceList').val();
	requestedCommand = document.getElementById('commandArea').value;
	$("#commandresult").text('Waiting to execute the shell command');
	$("textarea[name=output]").val('Wait for the output');
	$.post('/other/commands/' + deviceId, {
		command: requestedCommand
	},function (err, response) {
		getOutput(requestedCommand, globalCounter);
	});
}

/**
 * Repeatedly ask the server for the output of the executed command.
 * @param requestedCommand
 * @param counter
 */
function getOutput(requestedCommand, counter) {
	$.ajax({
		type: 'GET',
		url: '/other/output/' + deviceId + '/' + requestedCommand
	}).done(function (response) {
		if(response.retry === true) {
			setTimeout(function(){
				if(counter > 0) {
					getOutput(requestedCommand, counter);
				}
				else {
					counter--;
				}
			}, globalTimer);
		}
		else {
			$("textarea[name=output]").val(response.output);
			$("#commandresult").text(response.result);
		}
	});

}

/**
 * Send to the server the e-mail of the new delegated user.
 */
function delegation() {
	deviceId = $('#deviceList').val();
	delegateduser = document.getElementById('delegateduser').value;
	$.post('/other/delegation', {
		devId: deviceId,
		delegateduser: delegateduser
	},function (response) {
		if(response.error === '') {
			$("#delegationstatus").text('The user ' + delegateduser + ' was temporarily added as an administrator');
		}
		else
			$("#delegationstatus").text(response.error);
	});
}

/**
 * Change ownership of the device.
 */
function changeownership() {
	deviceId = $('#deviceList').val();
	newowner = document.getElementById('newowner').value;
	sharehistory = $('#sharehistory').val();
	$.post('/other/changeownership', {
		devId: deviceId,
		newowner: newowner,
		sharehistory: sharehistory
	},function (response) {
		if(response.error == '') {
			$("#ownershipstatus").text('The user ' + newowner + ' was added as owner of the device');
		}
		else
			$("#ownershipstatus").text(response.error);
	});
}

/**
 * Enable clicking between tabs in the UI.
 */
function setTabs() {
	jQuery('.tabs .tab-lists a').on('click', function(e)  {
		var currAttribute = jQuery(this).attr('href');
		jQuery('.tabs ' + currAttribute).show().siblings().hide();
		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

		if(currAttribute == "#expert") {
			populateConfigs($('#deviceList').val());
                }

		if(currAttribute == "#other") {
                        populateLogs($('#deviceList').val());
                        populateHistory($('#deviceList').val());
                }

		e.preventDefault();
	});
}

function setTabsData() {

}

/**
 * Reboot the device
 */
function reboot()
{
	deviceId = $('#deviceList').val();
	requestedCommand = 'reboot';
	$.post('/other/commands/' + deviceId, {
		command: requestedCommand
	},function (err, response) {
	});
}

/**
 * Disable all user input when the state of the device is LOCKED or FAILED_ONLINE
 */

function disableInput() {
	$("#tabs").find("input,button,textarea,select").attr("disabled", "disabled");
}

/**
 * Enable all the input once the state of the device returns to Normal.
 */
function enableInput() {
	$("#tabs").find("input,button,textarea,select").removeAttr('disabled');
}
