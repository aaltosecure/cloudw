var express = require('express');
var router = express.Router();
var passport = require('../utils/passport.js');

var Device = require('../model/device.js');
var ConfigChangeSet = require('../model/configChangeSet.js');
var Auth = require('./auth.js');
var DeviceType = require('../model/deviceType.js');
var KeyMappings = require('../model/keyMappings.js');
var async = require("async");
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var DeviceConfig = require('../model/deviceconfig.js');
var logger = require("../utils/logger.js");


/**
 * Send to the user interface the values for the keys in the EASY TAB.
 */
router.get('/easy/:devId', function(req, res, next) {
    /**
     * Create two arrays of the same length equal to the number of the input fields in the EASY TAB.
     * InputNames contains the names of the input fields in the EASY TAB and currValues contains the values from the
     * database in the respective position as InputNames.These arrays are used so that the processing can be iterated.
     * @type {string[]}
     */
    var inputNames = ['wirelessname', 'wpawirelesspw'];
    var currValues = ['', ''];

    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            DeviceConfig.all({where: {deviceId: req.params.devId}}, function(err, configs) {
                if(err) {
                    logger.writeLog('error', req.params.devId, null, err.toString());
                    res.status(500).send({msg: 'Cannot read device configuration'});
                }
                else {
                    Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                        if (err) {
                            logger.writeLog('error', req.params.devId, null, err.toString());
                            res.status(500).send({msg: 'Device does not exist'});
                        }
                        else {
                            DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {
                                if (err) {
                                    logger.writeLog('error', req.params.devId, null, err.toString());
                                    res.status(500).send({msg: 'This device is not supported'});
                                }
                                else {
                                    /**
                                     * For each input field in the inputNames array, asynchronously read the values from the database,
                                     * and write them in the respective position in the currValues array.
                                     */
                                    async.forEach(inputNames, function (inputName, callback) {
                                        KeyMappings.findOne({
                                            where: {
                                                inputName: inputName,
                                                deviceTypeId: type.id
                                            }
                                        }, function (err, keyMap) {
                                            if (err) {
                                                logger.writeLog('error', req.params.devId, null, err.toString());
                                                res.status(500).send({msg: 'Internal Server Error'});
                                            }
                                            else {
                                                var index = 0;
                                                /**
                                                 * Because it is an asynchronous call, we need to find the index of the inputName in
                                                 * the inputNames array.
                                                 */
                                                for (var i = 0; i < inputNames.length; i++) {
                                                    if (inputNames[i] === inputName) {
                                                        index = i;
                                                    }
                                                }
                                                currValues[index] = currentValues(configs, keyMap.key);
                                                callback();
                                            }
                                        });
                                    }, function (done) {
                                        /**
                                         * Send the values to the user interface.
                                         */
                                        res.send({
                                            wirelessname: currValues[0],
                                            wpawirelesspw: currValues[1]
                                        });
                                    });
                                }
                            })
                        }
                    })
                }
            })

        })
    })


});

/**
 * Send to the user interface the values for the keys in the NORMAL TAB.
 */
router.get('/normal/:devId', function(req, res, next) {
    /**
     * Create two arrays of the same length equal to the number of the key-value pairs in the NORMAL TAB.
     * InputNames contains the names of the input fields in the NORMAL TAB and currValues contains the values from the
     * database in the respective position as InputNames.These arrays are used so that the processing can be iterated.
     * @type {string[]}
     */
    var inputNames = ['routername', 'localip', 'subnetmask', 'defaultgw', 'localdns', 'wirelessname', 'wirelesspw', 'wlpasswtype', 'wlnetmode', 'ap', 'ssidbcast'];
    var currValues = ['', '', '', '', '', '', '', '', '', '', ''];
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            DeviceConfig.all({where: {deviceId: req.params.devId}}, function (err, configs) {
                if(err) {
                    logger.writeLog('error', req.params.devId, null, err.toString());
                    res.status(500).send({msg: 'Cannot read device configuration'});
                }
                else {
                    Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                        if (err) {
                            logger.writeLog('error', req.params.devId, null, err.toString());
                            res.status(500).send({msg: 'Device does not exist'});
                        }
                        else {
                            DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {
                                if (err) {
                                    logger.writeLog('error', req.params.devId, null, err.toString());
                                    res.status(500).send({msg: 'This device is not supported'});
                                }
                                else {
                                    /**
                                     * For each input field in the inputNames array, asynchronously read the values from the database,
                                     * and write them in the respective position in the currValues array.
                                     */
                                    async.forEach(inputNames, function (inputName, callback) {
                                        KeyMappings.findOne({
                                            where: {
                                                inputName: inputName,
                                                deviceTypeId: type.id
                                            }
                                        }, function (err, keyMap) {
                                            if (err) {
                                                logger.writeLog('error', req.params.devId, null, err.toString());
                                                res.status(500).send({msg: 'Internal Server Error'});
                                            }
                                            else {
                                                var index = 0;
                                                /**
                                                 * Because it is an asynchronous call, we need to find the index of the inputName in
                                                 * the inputNames array.
                                                 */
                                                for (var i = 0; i < inputNames.length; i++) {
                                                    if (inputNames[i] === inputName) {
                                                        index = i;
                                                    }
                                                }
                                                currValues[index] = currentValues(configs, keyMap.key);
                                                callback();
                                            }
                                        });
                                    }, function (done) {
                                        /**
                                         * Save the wireless password differently depending on the wireless type
                                         * @type {number}
                                         */
                                        var wlpasstypeIndex = 0;
                                        var wirelesspwIndex = 0;
                                        for (var i = 0; i < inputNames.length; i++) {
                                            if (inputNames[i] === 'wlpasstype') {
                                                wlpasstypeIndex = i;
                                            }
                                            if (inputNames[i] === 'wirelesspw') {
                                                wirelesspwIndex = i;
                                            }
                                        }
                                        switch (inputNames[wlpasstypeIndex]) {
                                            case 'none':
                                                inputNames[wirelesspwIndex] = 'nowirelesspw';
                                                break;
                                            case 'wep':
                                                inputNames[wirelesspwIndex] = 'wepwirelesspw';
                                                break;
                                            case 'wpa':
                                                inputNames[wirelesspwIndex] = 'wpawirelesspw';
                                                break;
                                            default:
                                                break;
                                        }
                                        KeyMappings.findOne({
                                            where: {inputName: inputNames[wirelesspwIndex], deviceTypeId: type.id}
                                        }, function (err, password) {
                                            if (err) {
                                                logger.writeLog('error', req.params.devId, null, err.toString());
                                                res.status(500).send({msg: 'Internal Server Error'});
                                            }
                                            else {
                                                currValues[wirelesspwIndex] = currentValues(configs, password.key);
                                                /**
                                                 * Send the values to the user interface.
                                                 */
                                                res.send({
                                                    routername: currValues[0],
                                                    localip: currValues[1],
                                                    subnetmask: currValues[2],
                                                    defaultgw: currValues[3],
                                                    localdns: currValues[4],
                                                    wirelessname: currValues[5],
                                                    wirelesspw: currValues[6],
                                                    wlpasstype: currValues[7],
                                                    wlnetmode: currValues[8],
                                                    ap: currValues[9],
                                                    ssidbcast: currValues[10]
                                                });
                                            }
                                        });

                                    });
                                }
                            })
                        }
                    });
                }
            })
        })
    });
});

/**
 * TODO: Code the functions to send the values from the server to the User Interface in the Advanced Tab.
 */
router.get('/advanced/setup/:devId', function(req, res, next) {

    var inputNames = ['routername', 'localip', 'subnetmask', 'defaultgw', 'localdns', 'hostname', 'domainname', 'mtu', 'dhcpip', 'maxdhcp', 'leasedhcp', 'sdnsone', 'sdnstwo', 'sdnsthree'];
    var currValues = ['', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            DeviceConfig.all({where: {deviceId: req.params.devId}}, function (err, configs) {
                DeviceType.findOne({where: {id: req.params.devId}}, function (err, type) {
                    async.forEach(inputNames, function (inputName, callback) {KeyMappings.findOne({where: {inputName: inputName, deviceTypeId: type.id}
                    }, function (err, keyMap) {
                        var index = 0;
                        for (var i = 0; i < inputNames.length; i++) {
                            if (inputNames[i] === inputName) {
                                index = i;
                            }
                        }
                        currValues[index] = currentValues(configs, keyMap.key);
                        callback();
                    });
                    }, function (done) {
                        res.send({
                            routername: currValues[0],
                            localip: currValues[1],
                            subnetmask: currValues[2],
                            defaultgw: currValues[3],
                            localdns: currValues[4],
                            hostname: currValues[5],
                            domainname: currValues[6],
                            mtu: currValues[7],
                            dhcpip: currValues[8],
                            maxdhcp: currValues[9],
                            leasedhcp: currValues[10],
                            sdnsone: currValues[11],
                            sdnstwo: currValues[12],
                            sdnsthree: currValues[13],
                        });
                    });
                });

            })
        })
    });
});

/**
 * TODO: Code the functions to send the values from the server to the User Interface in the Advanced Tab.
 */
router.get('/advanced/wireless/:devId', function(req, res, next) {
    var inputNames = ['wirelessname', 'wirelesspw'];
    var currValues = ['', ''];
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            DeviceConfig.all({where: {deviceId: req.params.devId}}, function(err, configs) {
                DeviceType.findOne({where: {id: req.params.devId}}, function (err, type) {
                    async.forEach(inputNames, function(inputName, callback) {
                        KeyMappings.findOne({where: {inputName: inputName, deviceTypeId: type.id}}, function (err, keyMap) {
                            var index = 0;
                            for (var i = 0; i < inputNames.length; i++) {
                                if (inputNames[i] === inputName) {
                                    index = i;
                                }
                            }
                            currValues[index] = currentValues(configs, keyMap.key);
                            callback();
                        });
                    }, function(done) {
                        res.send({
                            wirelessname: currValues[0],
                            wirelesspw: currValues[1]
                        });
                    });
                })
            })

        })
    })


});

/**
 * TODO: Code the functions to send the values from the server to the User Interface in the Advanced Tab.
 */
router.get('/advanced/services/:devId', function(req, res, next) {
    var inputNames = ['wirelessname', 'wirelesspw'];
    var currValues = ['', ''];
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            DeviceConfig.all({where: {deviceId: req.params.devId}}, function(err, configs) {
                DeviceType.findOne({where: {id: req.params.devId}}, function (err, type) {
                    async.forEach(inputNames, function(inputName, callback) {
                        KeyMappings.findOne({where: {inputName: inputName, deviceTypeId: type.id}}, function (err, keyMap) {
                            var index = 0;
                            for (var i = 0; i < inputNames.length; i++) {
                                if (inputNames[i] === inputName) {
                                    index = i;
                                }
                            }
                            currValues[index] = currentValues(configs, keyMap.key);
                            callback();
                        });
                    }, function(done) {
                        res.send({
                            wirelessname: currValues[0],
                            wirelesspw: currValues[1]
                        });
                    });
                })
            })

        })
    })


});

/**
 * Get the router and OS version from the database and send it to the user Interface.
 */
router.get('/versions/:devId', function(req, res, next) {

    Auth.isLoggedIn(req, res, function() {
        Auth.ownsDevice(req.params.devId, req, res, function () {
            Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                if (err) {
                    logger.writeLog('error', req.params.devId, null, err.toString());
                    res.status(500).send({msg: 'Device does not exist'});
                }
                else {
                    DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {
                        if (err) {
                            logger.writeLog('error', req.params.devId, null, err.toString());
                            res.status(500).send({msg: 'This device is not supported'});
                        }
                        else {
                            res.send({
                                routerversion: type.hardwareType,
                                osversion: type.softwareType
                            });
                        }

                    });
                }
            })
        })
    })
});

/**
 * Return the value of the corresponding key in the configs array.
 * @param configs
 * @param key
 * @returns {*}
 */

function currentValues(configs, key){
    for(var c in configs)
        if(configs.hasOwnProperty(c))
            if (configs[c].key === key)
                return configs[c].value;
    return '';
}

module.exports = router;