var express = require('express');
var passport = require('passport');
var router = express.Router();
var Device = require('../model/device.js');
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var User = require('../model/user.js');
var bCrypt = require('bcrypt-nodejs');
var logger = require("../utils/logger.js");

/**
 * Compares the hash and the password.
 * @param password
 * @param hash
 * @returns {*}
 */

var isValidPassword = function(password, hash){
    return bCrypt.compareSync(password, hash);
};

/**
 * Check if the password conforms the password policy
 * @param password
 * @returns {boolean}
 */
var passwordPolicy = function(password) {
    if(password.length < 8)
        return false;
    return true;
};

var emailPolicy = function (username) {
    var result = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return result.test(username);
};

/**
 * Checks if the logged in user owns the device with id equal to deviceId.
 * @param deviceId
 * @param req
 * @param res
 * @param next
 */

var ownsDevice = function ownsDevice(deviceId, req, res, next) {
    UserDeviceMapping.all({where: {deviceId: deviceId, userId: req.user.id}}, function (err, mappings) {
        if(mappings.length === 1) {
            if(err) {
                logger.writeLog('error', null, req.user.id, err.toString());
                res.status(500).send({msg: 'Internal Server Error'});
            }
            else {

                /**
                 * The logged in user can be the owner of the device or someone with delegated access rights to control
                 * the device. In the second case, the delegated user can access the page only if its access right has
                 * not expired.
                 */
                if (mappings[0].deviceAccessLevel === 'OWNERSHIP' || (mappings[0].deviceAccessLevel === 'ACCESSRIGHT' && new Date().getTime() < mappings[0].accessExpiryTime )) {
                    Device.findOne({where: {id: mappings[0].deviceId}}, function (err, device) {
                        if (err || !mappings) {
                            logger.writeLog('error', deviceId, req.user.id, err.toString());
                            res.send({msg: 'Failure'});
                        } else if (device.allowOffband == false && device.key != req.deviceFingerprint) {
                            res.status(403).send({msg: 'Modifying these configurations require connection through it'});
                        } else {
                            next();
                        }
                    });
                }
            }
        }
    });
};

/**
 * If the user is not logged in, he is redirected to the login page
 * @param req
 * @param res
 * @param next
 */

var isLoggedIn = function isLoggedIn(req, res, next) {
    if (! req.isAuthenticated())
        res.redirect('/login');
    else
        next();
};


/**
 * Authenticate user using the Facebook credentials.
 */
router.get('/facebook', passport.authenticate('facebook'));

/**
 * Provide a callback function when the Facebook authentication succeeded or not.
 */

router.get('/facebook/callback', passport.authenticate('facebook', {
        successRedirect: '../../',
        failureRedirect: '../login'
    }), function (req, res) {
        res.redirect('/');
    });

/**
 * Display the registration form for users who register without associating a device to them.
 */
router.get('/signup', function (req, res) {
    res.render('signup',
        { title: "Registration Form",
            message: req.flash('Welcome new user!')});
});

// Options for passport authentication
var ppOptions = {failureRedirect: '/auth/signup', failureFlash: true};

/**
 * Signup the user without associating a device to him.
 */
router.post('/signup', passport.authenticate('local-signup', ppOptions),
    function(req, res) {
        req.logout();
        res.redirect('../../login');
});


/**
 * Logout the user.
 */
router.get('../logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

/**
 * Redirect to the change password webpage.
 */
router.get('/changepw', function(req, res) {
   res.render('newpassword', {title: 'New Password', message:'Change password'});
});

/**
 * Change the user's password.
 */
router.post('/newpassword', function(req, res) {
    User.findOne({where: {id: req.user.id}}, function(err, us) {
        if(err) {
            logger.writeLog('error', null, req.user.id, err.toString());
            res.status(500).send({msg: 'User does not exist'});
        }
        else {
            if (isValidPassword(req.body.oldpassword, us.password) && req.body.password == req.body.confirmpassword && passwordPolicy(req.body.password)) {

                /**
                 * Save the new password in the database with a salt.
                 */
                us.password = bCrypt.hashSync(req.body.password, bCrypt.genSaltSync(10), null);
                us.save();
            }
            res.redirect('../../');
        }
    });
});

module.exports = router;
module.exports.isLoggedIn = isLoggedIn;
module.exports.ownsDevice = ownsDevice;
module.exports.passwordPolicy = passwordPolicy;
module.exports.emailPolicy = emailPolicy;
