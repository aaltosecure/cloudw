var express = require('express');
var router = express.Router();
var async = require("async");
var fileSystem = require('fs');
var fs = require('fs-extra');
var schema = require("../model/schema");
var Device = require('../model/device.js');
var DeviceAlertLog = require('../model/deviceAlertLog.js');
var DeviceConfig = require('../model/deviceconfig.js');
var DeviceConfigHistory = require('../model/deviceConfigHistory.js');
var ConfigChangeSet = require('../model/configChangeSet.js');
var DeviceHistory = require('../model/deviceHistory.js');
var DeviceType = require('../model/deviceType.js');
var ShellCommand = require('../model/shellCommandLog.js');
var OwnershipChangeRequest = require('../model/ownershipChangeRequest.js');
var logger = require("../utils/logger.js");

/**
 * The following section contains the device-cloud REST APIs as below
 * 1. GET /device/:id/cmd
 * 2. POST /device/:id/configs
 * 3. PUT /device/:id/configs
 * 4. POST /device/:id/alerts
 * 5. POST /device/:id/shellOutput
 * 6. POST /device/:id/uploadFile
 */

/**
 * REST-API for next command
 */
router.get('/:id/cmd',
    isAuthenticatedDevice,
    registerDevice,
    validateCall,
    getNextCommand
);

/**
 * REST-API to handle configuration POST
 */
router.post('/:id/configs', isAuthenticatedDevice, validateCall, function(req, res, next) {

    logger.writeLog('info', req.params.id, null, 'Called:' + req.originalUrl.toString());
    //Get the configuration key-value pairs from the request body
    var newConfigs = formatConfigs(req);

    //Get Timestamp for transaction
    var transactionTime = new Date().getTime();

    //This counter is used to take the device config snapshot
    //when all the key-value pairs have been added to the CurrentDeviceConfig table
    var counter=0;

    //Delete old configuration, if present, for the device.
    schema.client.query('DELETE FROM CurrentDeviceConfig WHERE deviceId = '+req.params.id, function(err, val) {
        if(err){
            //Handle error
            logger.writeLog('error', req.params.id, null, err.toString());
            res.status(500).send({msg: 'Internal Server Error'});
        }else{
            //Add new configurations inside a transaction
            schema.client.query("begin transaction");

            //Key-value pairs can be added to database asynchronously
            async.each(newConfigs, function(conf, callback) {
                var key = Object.keys(conf)[0];
                var value = conf[key];

                var deviceConfig = new DeviceConfig;
                deviceConfig.deviceId = req.params.id;
                deviceConfig.key = key;
                deviceConfig.value = value;
                deviceConfig.save(null, function(err, savedItem) {
                    if(err){
                        //Handle error
                        schema.client.query("rollback");
                        logger.writeLog('error', req.params.id, null, err.toString());
                        res.status(500).send({msg: 'Internal Server Error'});
                    }
                    counter++;
                    if(counter == newConfigs.length) //When all the key-value pairs have been added
                        takeDeviceConfigSnapshot(transactionTime, req.params.id);
                });
                callback();

            }, function(err) {//Called after all the key-value pairs have been added to database
                if(err) {
                    //Handle error
                    schema.client.query("rollback");
                    logger.writeLog('error', req.params.id, null, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                }else{
                    //success - commit transaction and send next command to device
                    logger.writeLog('debug', req.params.id, null, 'Device configuration updated');
                    schema.client.query("commit");
                    //The sleep below (for 2mins) is required as the device
                    //posts a lot of config key-value pairs and the
                    //jugglingdb connector takes a long time in the background to
                    //handle insertions in the database.
                    //Further database queries in the meantime may degrade
                    //the performance of the server.
                    deviceSleep(res, 60000);
                }
            });
        }
    });
});

/**
 * REST-API to handle configuration PUT
 */
router.put('/:id/configs', isAuthenticatedDevice, validateCall, validatePutConfig, function(req, res, next) {

    logger.writeLog('info', req.params.id, null, 'Called:' + req.originalUrl.toString());
    //Fetch all the configuration changes proposed by the user and acknowledged by the device
    ConfigChangeSet.all({where: {deviceId: req.params.id}}, function(err, confs){

        //Get Timestamp for transaction
        var transactionTime = new Date().getTime();

        //This counter is used to take the device config snapshot
        //when all the changes have been updated in the CurrentDeviceConfig table
        var counter=0;

        //The change set must be handled atomically. Hence a transaction should be used.
        schema.client.query("begin transaction");
        async.each(confs, function(conf, callback) {
            if(conf.value === null){ //NULL value indicates that the key must be deleted from the device config
                //Delete key from CurrentDeviceConfig
                schema.client.query("DELETE FROM CurrentDeviceConfig " +
                    "WHERE deviceId = "+req.params.id+" AND `key` = '"+conf.key+"'", function(err){
                        if(err){
                            //Handle error
                            schema.client.query("rollback");
                            logger.writeLog('error', req.params.id, null, err.toString());
                            res.status(500).send({msg: 'Internal Server Error'});
                        }
                        counter++;
                        if(counter == confs.length) //When all the changes have been updated in the CurrentDeviceConfig
                            takeDeviceConfigSnapshot(transactionTime, req.params.id);
                    });
            }else{
                //Insert/Update key in CurrentDeviceConfig
                //Check if the key already exists in the device configuration
                DeviceConfig.findOne({where: {key: conf.key, deviceId: req.params.id}}, function(err, deviceConfig) {
                    if(err || !deviceConfig) {//Configuration key not present in CurrentDeviceConfig table
                        //Create a new deviceConfig key-value pair in the CurrentDeviceConfig table
                        deviceConfig = new DeviceConfig;
                        deviceConfig.key = conf.key;
                        deviceConfig.deviceId = req.params.id;
                    }
                    deviceConfig.userId = conf.userId;//Configuration posted by the device, not user
                    deviceConfig.value = conf.value;
                    deviceConfig.save(null, function(err, savedItem) {
                        if(err){
                            //Handle error
                            schema.client.query("rollback");
                            logger.writeLog('error', req.params.id, null, err.toString());
                            res.status(500).send({msg: 'Internal Server Error'});
                        }
                        counter++;
                        if(counter == confs.length) //When all the changes have been updated in the CurrentDeviceConfig
                            takeDeviceConfigSnapshot(transactionTime, req.params.id);
                    });
                });
            }
            callback();
        }, function(err) {//Called after all the changes have been updated in CurrentDeviceConfig table
            if(err){
                schema.client.query("rollback");
                logger.writeLog('error', req.params.id, null, err.toString());
                res.status(500).send({msg: 'Internal Server Error'});
            }else{
                schema.client.query("commit");
                logger.writeLog('debug', req.params.id, null, 'Device configuration updated');
                //Delete ConfigChangeSet for device
                schema.client.query('DELETE FROM ConfigChangeSet WHERE deviceId = '+req.params.id, function(err){
                    if(err){
                        logger.writeLog('error', req.params.id, null, err.toString());
                    }else{
                        logger.writeLog('debug', req.params.id, null, 'Config change set deleted');
                    }
                });

                //If device is locked, unlock device and insert row in Device History
                Device.findOne({where: {id: req.params.id}}, function(err, device){
                    if(device.state === 'LOCKED'){//Device is locked
                        //Unlock device
                        device.state = 'NORMAL';
                        device.save(null, function(err) {
                            //Insert row in Device History
                            if(err){
                                logger.writeLog('error', req.params.id, null, err.toString());
                            }else{
                                logger.writeLog('debug', req.params.id, null, 'Device set to NORMAL state');
                                saveDeviceHistory(transactionTime, device);
                            }
                        });
                    }
                });

                //Send next command to device
                next();
            }
        });
    });
}, getNextCommand);

/**
 * REST-API to handle alert log from devices
 */
router.post('/:id/alerts', isAuthenticatedDevice, validateCall, function(req, res, next) {

    logger.writeLog('info', req.params.id, null, 'Called:' + req.originalUrl.toString());
    logger.writeLog('debug', req.params.id, null,
        'alertCode: ' + req.body.alertCode + ', alertMessage: ' + req.body.alertMessage);

    //AlertCode must be present in the request body
    if(!req.body.alertCode) {
        //Invalid parameters
        logger.writeLog('error', req.params.id, null, 'Invalid parameters');
        res.status(400).send({msg: 'Bad Request'});
    }else {
        //Insert alert from device into the DeviceAlertLog table
        var alertLog = new DeviceAlertLog;
        alertLog.alertCode = req.body.alertCode;
        alertLog.msg = req.body.alertMessage;
        alertLog.deviceId = req.params.id;
        alertLog.timestamp = new Date().getTime();
        alertLog.save(null, function(err) {
            if(err){
                //internal error
                logger.writeLog('error', req.params.id, null, err.toString());
            }else{
                logger.writeLog('debug', req.params.id, null, 'Alert successfully received');
            }
        });
        res.status(200).send({
            cmds: [
                {
                    action: 'callGetCmd',
                    data: ''
                }
            ]
        });
    }
});

/**
 * REST-API to handle command execution output from devices
 */
router.post('/:id/shellOutput', isAuthenticatedDevice, validateCall, function(req, res, next) {

    logger.writeLog('info', req.params.id, null, 'Called:' + req.originalUrl.toString());
    logger.writeLog('debug', req.params.id, null,
        'instruction: ' + req.body.instruction + ', output: ' + req.body.output);

    //instruction is required in the requst body to identify the corresponding shell command
    //output is the output of executing the shell command locally in the device
    if(!req.body.instruction || !req.body.output){
        //Invalid parameters
        logger.writeLog('error', req.params.id, null, 'Invalid parameters');
        res.status(400).send({msg: 'Bad Request'});
    }else{
        var execOutput = req.body.output.toString();

        //Output can be at most 250 characters long
        if(execOutput.length > 250){
            logger.writeLog('debug', req.params.id, null, 'Output too long -> truncated');
            execOutput = execOutput.substr(0,250);
        }

        //Update output in ShellCommandLog table for the corresponding device and shell command
        schema.client.query(
            "UPDATE ShellCommandLog SET output='"+execOutput+"' " +
            "WHERE deviceId="+req.params.id+" AND shellCommand='"+req.body.instruction+"' " +
            "AND output IS NULL AND cmdRequestTimestamp IS NOT NULL", function(err){
                if(err){
                    //internal error
                    logger.writeLog('error', req.params.id, null, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                }else{
                    //success - send next command to device
                    logger.writeLog('debug', req.params.id, null,
                        'Shell command execution output successfully received');
                    next();
                }
            });

    }
}, getNextCommand);

/**
 * REST-API to upload files to server
 */
router.post('/:id/uploadFile', isAuthenticatedDevice, validateCall, function(req, res, next) {
    logger.writeLog('info', req.params.id, null, 'Called:' + req.originalUrl.toString());
    var fstream;
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        logger.writeLog('debug', req.params.id, null,
            'Name of the file to be uploaded: ' + filename);
        //Create sub-folder for deviceId within deviceFiles folder
        fileSystem.mkdir('deviceFiles/' + req.params.id, 0600, function(err) {
            var timestamp = new Date().getTime();
            //Create timestamp specific sub-folder
            fileSystem.mkdir('deviceFiles/' + req.params.id + '/' + timestamp, 0600, function(err) {
                if(err){
                    logger.writeLog('error', req.params.id, null, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                }else{
                    fstream = fs.createWriteStream('deviceFiles/' + req.params.id + '/' + timestamp + '/' + filename);
                    file.pipe(fstream);
                    fstream.on('close', function () {
                        logger.writeLog('debug', req.params.id, null,
                            'File uploaded successfully');
                        //Update Device->isDebugInfoRequested flag
                        Device.findOne({where: {id: req.params.id}}, function(err, device) {
                            if (device.isDebugInfoRequested === true) {//Debugging info requested
                                device.isDebugInfoRequested = false;//Reset
                                device.save(null, function (err) {
                                    if(err){
                                        logger.writeLog('error', req.params.id, null, err.toString());
                                    }
                                });
                            }
                        });
                        res.status(200).send({
                            cmds: [
                                {
                                    action: 'callGetCmd',
                                    data: ''
                                }
                            ]
                        });
                    });
                }
            });
        });
    });
});

//Verifies if PUT Configs is valid
function validatePutConfig(req, res, next){
    Device.findOne({where: {id: req.params.id}}, function(err, device){
        if(err){
            logger.writeLog('error', req.params.id, null, err.toString());
        }
        if(device.state !== 'LOCKED'){
            logger.writeLog('error', req.params.id, null, 'Device is not in the LOCKED state');
            res.status(400).send({msg: 'Bad Request'});
        }else{
            //Get the configuration key-value pairs from the request body
            var newConfigs = formatConfigs(req);
            //Fetch all the configuration changes requested by user
            ConfigChangeSet.all({where: {deviceId: req.params.id}}, function(err, changes){
                if(err) {
                    logger.writeLog('error', req.params.id, null, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                } else {
                    //console.log('changes.length: '+changes.length);
                    var updateCount = 0;
                    var deleteCount = 0;
                    for(var i=0; i<changes.length; i++){
                        //console.log('changes[i].value: '+ changes[i].value);
                        if(changes[i].value === null){ //NULL value: the key must be deleted from the device config
                            var isKeyPresentInPut = 0;
                            for(var j=0; j<newConfigs.length; j++){ //Checks if the key is present in the PUT request
                                var key = Object.keys(newConfigs[j])[0];
                                if(key === changes[i].key){ //Match found
                                    isKeyPresentInPut = 1;
                                    break;
                                }
                            }
                            if(!isKeyPresentInPut) //Key not present in PUT => Key has been deleted by the device
                                deleteCount++;
                            continue;
                        }
                        for(var j=0; j<newConfigs.length; j++){ //Checks if keys have updated values
                            var key = Object.keys(newConfigs[j])[0];
                            var value = newConfigs[j][key];
                            //console.log('key: '+ key);
                            if(key === changes[i].key && value === changes[i].value){ //Match found
                                updateCount++;
                                break;
                            }
                        }
                    }
                    //console.log('updateCount: '+updateCount);
                    //console.log('deleteCount: '+deleteCount);

                    //Check if all changes have been acknowledged by the device
                    if (changes.length > 0 && changes.length === (updateCount + deleteCount)){
                        logger.writeLog('debug', req.params.id, null, 'PUT request validated successfully');
                        next();
                    }else{//Invalid PUT
                        logger.writeLog('debug', req.params.id, null, 'Invalid PUT request data');
                        Device.findOne({where: {id: req.params.id}}, function(err, device) {
                            device.state = 'FAILED_ONLINE';
                            device.save(null, function(err) {
                                if(err){
                                    logger.writeLog('error', req.params.id, null, err.toString());
                                }else{
                                    var timestamp = new Date().getTime();
                                    saveDeviceHistory(timestamp, device);
                                }
                            });
                        });
                        res.status(400).send({msg: 'Bad Request'});
                    }
                }
            });
        }
    });
}

//Formats the key-value pairs in request body
function formatConfigs(req){
    var newConfigs = [];
    //console.log(req.body);
    for(var conf in req.body) {
        var newConfig= {};
        newConfig[conf] = req.body[conf];
        //console.log(conf + ' : ' + newConfig[conf]);
        newConfigs.push(newConfig);
    }
    return newConfigs;
}

//Takes Device Snapshot
function takeDeviceConfigSnapshot(timestamp, deviceId){
    schema.client.query(
        "select `userId`, `key`, `value` from `CurrentDeviceConfig` where `deviceId` = " + deviceId, function(err, items){
            if(err){
                logger.writeLog('error', deviceId, null, err.toString());
            }else{
                var snapshot = JSON.stringify(items);
                //console.log(snapshot);
                var deviceConfigHistory = new DeviceConfigHistory;
                deviceConfigHistory.deviceId = deviceId;
                deviceConfigHistory.snapshot = snapshot;
                deviceConfigHistory.timestamp = timestamp;
                deviceConfigHistory.save(null, function(err) {
                    if(err){
                        logger.writeLog('error', deviceId, null, err.toString());
                    }
                });
            }
        }
    );
}

//Saves Device History in the DeviceHistory table
function saveDeviceHistory(timestamp, device){
    var deviceHistory = new DeviceHistory;
    deviceHistory.deviceId = device.id;
    deviceHistory.key = device.key;
    deviceHistory.allowOffband = device.allowOffband;
    deviceHistory.deviceTypeId = device.deviceTypeId;
    deviceHistory.state = device.state;
    deviceHistory.lastLockTime = device.lastLockTime;
    deviceHistory.timestamp = timestamp;
    deviceHistory.save(null, function(err, savedItem) {
        if(err){
            logger.writeLog('error', device.id, null, err.toString());
        }else{
            logger.writeLog('debug', deviceId, null, 'Device History updated');
        }
    });
}

//Ask device to sleep for 'sleepTime' milliseconds
function deviceSleep(res, sleepTime){
    res.status(200).send({
        cmds: [
            {
                action: 'localSleep',
                data: sleepTime
            },
            {
                action: 'callGetCmd',
                data: ''
            }
        ]
    });
}

//Checks that the device is actually the device it claims to be
//Used if the device knows its Id
function validateCall(req, res, next) {
    if(!req.connection.getPeerCertificate()) { //If the device does not have a certificate
        logger.writeLog('error', req.params.id, null, 'Authentication failure');
        res.status(401).send({msg: 'Unauthorized'});
    } else{
        Device.find(req.params.id, function(err, device) {
            if(err) {
                logger.writeLog('error', req.params.id, null, err.toString());
                res.status(500).send({msg: 'Internal Server Error'});
            } else if(!device) { //If the device Id is not found in the Device table
                logger.writeLog('error', req.params.id, null, 'Device not found');
                res.status(400).send({msg: 'Bad Request'});
            } else if(device.key != req.connection.getPeerCertificate().fingerprint) { //If fingerprint does not match
                logger.writeLog('error', req.params.id, null,
                    'Certificate fingerprint does not match with device fingerprint');
                res.status(403).send({msg: 'Forbidden'});
            } else
                next();
        });
    }
}

//Checks that the connection comes through TLS and the client has a valid certificate
//Used if the device does not know its Id
function isAuthenticatedDevice(req, res, next) {
    if(req.client.authorized) {
        next();
    } else{
        logger.writeLog('error', req.params.id, null, 'Authentication failure');
        res.status(401).send({msg: 'Unauthorized'});
    }
}

//If id=0, the device fingerprint is used to register device
function registerDevice(req, res, next) {
    if(req.params.id == 0) {  //If device does not know its Id
        var fingerprint = req.connection.getPeerCertificate().fingerprint;
        //var fingerprint = '44444'; //Hard-coding used for testing purpose

        logger.writeLog('debug', req.params.id, null, 'Cert fingerprint: ' + fingerprint);
        var timestamp = new Date().getTime();
        //Trying to find device based on the cert fingerprint
        Device.findOne({where: {key: fingerprint}}, function(err, device){
            if(err) {
                logger.writeLog('error', req.params.id, null, err.toString());
                res.status(500).send({msg: 'Internal Server Error'});
            }else if(!device){ //If no device with the fingerprint is registered yet

                //console.log('hardwareType: ' + req.query.hardwareType);
                //console.log('softwareType: ' + req.query.softwareType);
                logger.writeLog('debug', req.params.id, null, 'Device with same fingerprint not found');

                //If Id=0, the query-string must have hardwareType and softwareType parameters
                if(req.query.hardwareType == null || req.query.softwareType == null){
                    logger.writeLog('error', req.params.id, null, 'Invalid parameters');
                    res.status(400).send({msg: 'Bad Request'});
                }else{

                    logger.writeLog('debug', req.params.id, null,
                        'hardwareType: ' + req.query.hardwareType + ', softwareType: ' + req.query.softwareType);
                    //Find deviceType based on hardwareType and softwareType
                    DeviceType.findOne({where: {hardwareType: req.query.hardwareType,
                            softwareType: req.query.softwareType}},
                        function(err, devType){
                            if(err){
                                logger.writeLog('error', req.params.id, null, err.toString());
                                res.status(500).send({msg: 'Internal Server Error'});
                            }else if(!devType){
                                logger.writeLog('error', req.params.id, null, 'Unknown device type');
                                res.status(400).send({msg: 'Bad Request'});
                            }else{

                                logger.writeLog('debug', req.params.id, null,
                                    'Creating new device with fingerprint: ' + fingerprint);
                                //Create a new Device to be registered
                                var newDevice = new Device;
                                newDevice.key = fingerprint;
                                newDevice.allowOffband = false;
                                newDevice.deviceTypeId = devType.id;
                                newDevice.state = 'NORMAL';
                                newDevice.isDebugInfoRequested = false;
                                newDevice.save(null, function(err, savedItem) {
                                    if(err){
                                        logger.writeLog('error', req.params.id, null, err.toString());
                                        res.status(500).send({msg: 'Internal Server Error'});
                                    }else{
                                        //Insert a row in the DeviceHistory table
                                        logger.writeLog('debug', req.params.id, null,
                                            'Device created successfully with id: ' + savedItem.id);
                                        saveDeviceHistory(timestamp, savedItem);

                                        res.status(200).send({
                                            cmds: [
                                                {
                                                    action: 'localUpdateDeviceId',
                                                    data: savedItem.id
                                                },
                                                {
                                                    action: 'callPostConfigs',
                                                    data: ''
                                                }
                                            ]
                                        });
                                    }
                                });
                            }
                        });
                }
            }else{ //If a device with the same fingerprint is already registered
                logger.writeLog('debug', device.id, null, 'Device with same fingerprint found');
                //Check if there is a pending ownership handover
                OwnershipChangeRequest.count({old_DeviceId: device.id, isHandled: false}, function(err, count){
                    if(err){
                        logger.writeLog('error', device.id, null, err.toString());
                        res.status(500).send({msg: 'Internal Server Error'});
                    }else if(count == 0){ //No pending ownership handover found
                        logger.writeLog('debug', device.id, null, 'No pending ownership handover found');
                        //Check if device is in RESET-STAGE2
                        if(device.state === 'RESET-STAGE2'){
                            logger.writeLog('debug', device.id, null, 'Device reset already handled');
                            //Set Device state to 'NORMAL'
                            device.state = 'NORMAL';
                            device.save(null, function(err) {
                                if(err){
                                    logger.writeLog('error', device.id, null, err.toString());
                                }else{
                                    logger.writeLog('debug', device.id, null, 'Device is reset to NORMAL state');
                                    saveDeviceHistory(timestamp, device);
                                }
                            });
                            //Send Device ID to device along with next suitable call-command
                            //Check if the configuration for the registered device is present
                            DeviceConfig.count({deviceId: device.id}, function(err, count){
                                if(err){
                                    logger.writeLog('error', device.id, null, err.toString());
                                    res.status(500).send({msg: 'Internal Server Error'});
                                }else if(count == 0){ //If config not present, the device is requested to post the config
                                    logger.writeLog('debug', device.id, null, 'Configuration not found. Device to post');
                                    res.status(200).send({
                                        cmds: [
                                            {
                                                action: 'localUpdateDeviceId',
                                                data: device.id
                                            },
                                            {
                                                action: 'callPostConfigs',
                                                data: ''
                                            }
                                        ]
                                    });
                                }else{ //If config already present, the device does not need to post the config again
                                    logger.writeLog('debug', device.id, null, 'Configuration found');
                                    res.status(200).send({
                                        cmds: [
                                            {
                                                action: 'localUpdateDeviceId',
                                                data: device.id
                                            },
                                            {
                                                action: 'callGetCmd',
                                                data: ''
                                            }
                                        ]
                                    });
                                }
                            });
                        }else{//Device RESET is yet to be handled
                            //Set Device state to 'RESET-STAGE1'
                            logger.writeLog('debug', device.id, null, 'Device reset yet to be handled. Device to sleep');
                            device.state = 'RESET-STAGE1';
                            device.save(null, function(err) {
                                if(err){
                                    logger.writeLog('error', device.id, null, err.toString());
                                }else{
                                    logger.writeLog('debug', device.id, null, 'Device is set to RESET-STAGE1 state');
                                    saveDeviceHistory(timestamp, device);
                                }
                            });
                            //Ask device to Sleep for 30 seconds
                            deviceSleep(res, 5000);
                        }
                    }else{//Pending ownership handover found
                        //Ask device to Sleep for 30 seconds
                        logger.writeLog('debug', device.id, null, 'Pending ownership handover found. Device to sleep');
                        deviceSleep(res, 5000);
                    }
                });
            }
        });
    } else //If device knows its Id, search for the next command to be sent to the device
        next();
}

//This function tries to find the next command(s) to be sent to a device
function getNextCommand(req, res, next) {
    //Check if the configuration for the registered device is present
    logger.writeLog('debug', req.params.id, null, 'Trying to find next command');
    DeviceConfig.count({deviceId: req.params.id}, function(err, count){
        if(err){
            logger.writeLog('error', req.params.id, null, err.toString());
            res.status(500).send({msg: 'Internal Server Error'});
        }else if(count == 0){ //Configuration not present
            logger.writeLog('debug', req.params.id, null, 'Configuration not found. Device to post.');
            //The device is requested to post the config
            res.status(200).send({
                cmds: [
                    {
                        action: 'callPostConfigs',
                        data: ''
                    }
                ]
            });
        }else{//Configuration for the device present, check for other conditions
            //Check ConfigChangeSet table (delta table which holds the user requests)
            ConfigChangeSet.all({where: {deviceId: req.params.id}}, function(err, configs){
                if(err){
                    logger.writeLog('error', req.params.id, null, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                }else{
                    Device.findOne({where: {id: req.params.id}}, function(err, device) {
                        if(err){
                            logger.writeLog('error', req.params.id, null, err.toString());
                        }
                        if(configs.length > 0 && //If user request present
                            (device.state === 'PUSH_REQUESTED' //Configuration changes to be pushed
                                || device.state === 'LOCKED')){//Device is already LOCKED
                            var cmds = [];
                            var updateKeys = [];
                            var delKeys = [];
                            var putKeys = [];
                            for(var i=0; i<configs.length; i++){
                                //console.log('configs[i].key: '+ configs[i].key);
                                //console.log('configs[i].value: '+ configs[i].value);
                                if(configs[i].value != null){//If keys need to be updated/inserted
                                    updateKeys.push({key: configs[i].key,value: configs[i].value});
                                }else{//If keys need to be deleted
                                    delKeys.push({key: configs[i].key});
                                }
                                putKeys.push({key: configs[i].key});
                            }
                            //console.log('updateKeys.length: '+ updateKeys.length);
                            //console.log('delKeys.length: '+ delKeys.length);
                            if(device.state === 'PUSH_REQUESTED'){
                                if(updateKeys.length > 0){
                                    logger.writeLog('debug', req.params.id, null, 'Keys to be updated');
                                    cmds.push({action: 'localUpdateConfig',data: updateKeys});
                                }
                                if(delKeys.length > 0){
                                    logger.writeLog('debug', req.params.id, null, 'Keys to be deleted');
                                    cmds.push({action: 'localDeleteConfig',data: delKeys});
                                }
                                logger.writeLog('debug', req.params.id, null, 'Device to be rebooted');
                                cmds.push({action: 'localReboot',data: ''});

                                //Lock device
                                device.state = 'LOCKED';
                                var timestamp = new Date().getTime();
                                device.lastLockTime = timestamp;

                                device.save(null, function(err) {
                                    if(err){
                                        logger.writeLog('error', req.params.id, null, err.toString());
                                    }else{
                                        logger.writeLog('debug', req.params.id, null, 'Device is LOCKED');
                                        saveDeviceHistory(timestamp, device);
                                    }
                                });
                            }else{//Device is already LOCKED
                                logger.writeLog('debug', req.params.id, null, 'Device is already LOCKED. Keys to be verified');
                                cmds.push({action: 'callPutConfigs',data: putKeys});
                            }
                            //Send commands
                            //console.log('commands: '+ JSON.stringify(cmds));
                            res.status(200).send({
                                cmds: cmds
                            });

                        }else{
                            //Check if Shell Command to be run on device
                            ShellCommand.findOne({where: {deviceId: req.params.id, output: null}, order: 'id ASC'},
                                function(err, shellCommand) {
                                    if(err){
                                        logger.writeLog('error', req.params.id, null, err.toString());
                                    }
                                    if(shellCommand){
                                        //Update cmdRequestTimestamp if null
                                        if(shellCommand.cmdRequestTimestamp == null){
                                            var timestamp = new Date().getTime();
                                            schema.client.query("UPDATE ShellCommandLog " +
                                                "set cmdRequestTimestamp = "+timestamp+" where id = "+shellCommand.id, function(err, val) {
                                                if(err){
                                                    logger.writeLog('error', req.params.id, null, err.toString());
                                                }
                                            });
                                            /*shellCommand.save(null, function(err, savedItem) {
                                                if(err){
                                                    logger.writeLog('error', req.params.id, null, err.toString());
                                                }
                                            });*/
                                        }
                                        logger.writeLog('debug', req.params.id, null,
                                            'Shell command to be executed: '+ shellCommand.shellCommand);
                                        //Send Shell Command to the device to be executed
                                        res.status(200).send({
                                            cmds: [
                                                {
                                                    action: 'callPostShellOutput',
                                                    data: shellCommand.shellCommand
                                                }
                                            ]
                                        });

                                    }else{
                                        //Check if the device needs to send debugging-info file
                                        logger.writeLog('debug', req.params.id, null,
                                            'isDebugInfoRequested: '+ device.isDebugInfoRequested);
                                        if(device.isDebugInfoRequested === true){
                                            //Get the device type details
                                            DeviceType.findOne({where: {id: device.deviceTypeId}}, function(err, deviceType) {
                                                if(err){
                                                    logger.writeLog('error', req.params.id, null, err.toString());
                                                }
                                                var fileName = 'debugScript_' + deviceType.hardwareType + '_' + deviceType.softwareType + '.sh';
                                                res.send({
                                                    cmds: [
                                                        {
                                                            action: 'downloadScript',
                                                            data: fileName
                                                        },
                                                        {
                                                            action: 'callPostUploadFile',
                                                            data: ''
                                                        }
                                                    ]
                                                });
                                            });
                                        }else{
                                            //The device has nothing to do at the moment. It may sleep for a while!
                                            //Ask device to Sleep for 30 seconds
                                            logger.writeLog('debug', req.params.id, null,
                                                'No pending task found. Device to sleep.');
                                            deviceSleep(res, 5000);
                                        }
                                    }
                                });
                        }
                    });
                }
            });
        }
    });
}

module.exports = router;
module.exports.saveDeviceHistory = saveDeviceHistory;
module.exports.takeDeviceConfigSnapshot = takeDeviceConfigSnapshot;
