var express = require('express');
var router = express.Router();
var User = require('../model/user.js');
var Device = require('../model/device.js');
var KeyCache = require('../model/keyCache.js');
var ConfigChangeSet = require('../model/configChangeSet.js');
var passport = require('../utils/passport.js');
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var CurrentDeviceConfig = require('../model/deviceconfig.js');
var Auth = require('./auth.js');
var pushRequestedState = 'PUSH_REQUESTED';
var logger = require("../utils/logger.js");
var normalState = 'NORMAL';
var async = require("async");

/* GET devices list belonging to the logged in user who can be permanent or temporary owner */
router.get('/list', function(req, res, next) {
	Auth.isLoggedIn(req, res, function() {
		UserDeviceMapping.all({where: {userId: req.user.id}}, function (err, mappings) {
			if(err) {
				logger.writeLog('error', null, req.user.id, err.toString());
				res.status(500).send({msg: 'Device does not belong to the user'});
			}
			else {
				var devices = [];
				/**
				 * Add a device in the response only if the logged in user is the owner of the device or he has been
				 * granted temporary access.
				 */
				async.forEach(mappings, function(mapping, callback) {
					if (mapping.deviceAccessLevel === 'OWNERSHIP' || (mapping.deviceAccessLevel === 'ACCESSRIGHT' && new Date().getTime() < mapping.accessExpiryTime )) {
						Device.findOne({where: {id: mapping.deviceId}}, function (err, device) {
							if (err) {
								logger.writeLog('error', null, req.user.id, err.toString());
								res.status(500).send({msg: 'Device does not exist'});
							}
							else {
								for (var i = 0, len = KeyCache.types.length; i < len; i++) {
									if (KeyCache.types[i].id == device.id) {
										device.hardwareType = KeyCache.types[i].hardwareType;
										device.softwareType = KeyCache.types[i].softwareType;
									}
								}
								devices.push(device);
							}
							callback();
						})
					}
				}, function(done) {
					res.json(devices);
				})
			}
		});
	});
});


/**
 * Get the current configuration for the device with id equal to devId.
 */
router.get('/listConfigs/:devId', function(req, res, next) {

	Auth.isLoggedIn(req, res, function() {
		Auth.ownsDevice(req.params.devId, req, res, function () {
			CurrentDeviceConfig.all({where: {deviceId: req.params.devId}}, function (err, configs) {
				if(err) {
					logger.writeLog('error', req.params.devId, req.user.id, err.toString());
					res.status(500).send({msg: 'Cannot read device configuration'});
				}
				else {
					res.json(configs);
				}
			});
		})
	})
});


/**
 * Delete a configuration key-value pair for the device with id equal to devId. When the user deletes a key,
 * a new entry with value equal to null is saved in the ConfigChangeSet table in the database.
 */
router.post('/deleteConfig', function(req, res, next) {
	Auth.isLoggedIn(req, res, function(){
		Auth.ownsDevice(req.body.devId, req, res, function() {
			ConfigChangeSet.findOne({where: {key: req.body.key, userId: req.user.id, deviceId: req.body.devId}}, function (err, config) {
				if(err) {
					logger.writeLog('error', req.body.devId, req.user.id, err.toString());
					res.status(500).send({msg: 'Internal Server Error'});
				}
				else {
					Device.findOne({where: {id: req.body.devId}}, function (err, device) {
						if (err || !device) {
							logger.writeLog('error', null, null, err.toString());
							res.status(500).send({msg: 'Device does not exist'});
						}
						else if (device.state === normalState || device.state === pushRequestedState) {

							/**
							 * Set the state of the device to PUSH_REQUESTED
							 */
							setPushRequested(req.body.devId);
							if (config == null) {

								/**
								 * Notice that the value field is not written so that it is saved as null.
								 */
								newConf = new ConfigChangeSet;
								newConf.userId = req.user.id;
								newConf.key = req.body.key;
								newConf.deviceId = req.body.devId;
								newConf.save(null, function (err, savedItem) {
									if (err) {
										logger.writeLog('error', req.body.devId, req.user.id, err.toString());
										res.status(500).send({msg: 'Configuration could not be saved'});
									}
								});
								res.send({error: ''});
							}
							else {

								/**
								 * There might be some cases where the user updates a key-value pair and before the changes
								 * are applied to the router, he deletes it. Therefore the entry in the ConfigChangeSet regarding
								 * that key-value pair is overwritten to null.
								 */
								config.updateAttribute('value', null, function (err) {
									if (err) {
										logger.writeLog('error', null, req.user.id, err.toString());
										res.status(500).send({msg: 'Configuration could not be saved'});
									}
									else {
										res.send({error: ''});
									}
								});
							}
						}
						else {
							res.send({error: 'Changes were not saved because the device is locked'});
						}
					})
				}
			});
		})
	})
});

/**
 * The user edits a key value pair for the device with id equal to devId.
 */
 router.post('/editConfig', function(req, res, next) {
	 Auth.isLoggedIn(req, res, function(){
		 Auth.ownsDevice(req.body.devId, req, res, function() {
			 ConfigChangeSet.findOne({where: {key: req.body.key, userId: req.user.id, deviceId: req.body.devId}}, function (err, config) {
				 if(err) {
					 logger.writeLog('error', req.body.devId, req.user.id, err.toString());
					 res.status(500).send({msg: 'Internal Server Error'});
				 }
				 else {

					 Device.findOne({where: {id: req.body.devId}}, function (err, device) {
						 if (err || !device) {
							 logger.writeLog('error', null, req.user.id, err.toString());
							 res.status(500).send({msg: 'Device does not exist'});
						 }
						 else if (device.state === normalState || device.state === pushRequestedState) {
							 /**
							  * Set the state of the device to PUSH_REQUESTED
							  */
							 setPushRequested(req.body.devId);

							 /**
							  * If there is no entry in the table regarding the edited key for the device with id equal to devId,
							  * then create a new row in the table.
							  */
							 if (config == null) {
								 newConf = new ConfigChangeSet;
								 newConf.userId = req.user.id;
								 newConf.key = req.body.key;
								 newConf.value = req.body.newValue;
								 newConf.deviceId = req.body.devId;
								 newConf.save(null, function (err, savedItem) {
									 if (err) {
										 logger.writeLog('error', req.body.devId, req.user.id, err.toString());
										 res.status(500).send({msg: 'Configuration could not be saved'});
									 }
								 });
								 res.send({error: ''});
							 }
							 else {
								 /**
								  * In some cases, the user edits the key-value pair and before the changes are applied in the router,
								  * he edits it again. Therefore the entry in the ConfigChangeSet regarding that key-value pair,
								  * is updated to the new value.
								  */
								 config.updateAttribute('value', req.body.newValue, function (err) {
									 if (err) {
										 logger.writeLog('error', null, req.user.id, err.toString());
										 res.status(500).send({msg: 'Configuration could not be saved'});
									 }
									 else {
										 res.send({error: ''});
									 }
								 });
							 }
						 }
						 else {
							 res.send({error: 'Changes were not saved because the device is locked'});
						 }
					 })
				 }
			 });
		 })
	 })
 });

/**
 * Get a JSON object containing information about the device with id equal to devId.
 */
router.get('/:devId', function(req, res, next) {
	Auth.isLoggedIn(req, res, function() {
		Auth.ownsDevice(req.params.devId, req, res, function () {
			Device.find(req.params.devId, function (err, device) {
				if(err) {
					logger.writeLog('error', req.params.devId, req.user.id, err.toString());
					res.status(500).send({msg: 'Device does not exist'});
				}
				else {
					res.json(device);
				}
			});
		})
	})
});


/**
 * Updates the allowOffBand field for the device with id equal to devId.
 */
router.post('/:devId', function(req, res, next) {
	Auth.isLoggedIn(req, res, function() {
		Auth.ownsDevice(req.params.devId, req, res, function () {
			Device.find(req.params.devId, function (err, device) {
				if(err) {
					logger.writeLog('error', req.params.devId, req.user.id, err.toString());
					res.status(500).send({msg: 'Internal Server Error'});
				}
				else {
					// TODO input validation (for the model)
					device.allowOffband = req.body.allowOffband;
					device.save(null, function (err, savedItem) {
						if(err) {
							logger.writeLog('error', req.params.devId, req.user.id, err.toString());
							res.status(500).send({msg: 'Changes could not be saved'});
						}
					});
					res.send({msg: ''});
				}
			});
		})
	})
});


/**
 * Update the state of device to PUSH_REQUESTED.
 * @param deviceId
 */
function setPushRequested(deviceId) {
	Device.findOne({where: {id: deviceId}}, function (err, device) {
		if(err) {
			logger.writeLog('error', deviceId, req.user.id, err.toString());
			res.status(500).send({msg: 'Device does not exist'});
		}
		else {
			device.updateAttribute('state', pushRequestedState, function (err) {
				if (err) {
					logger.writeLog('error', deviceId, req.user.id, err.toString());
					res.status(500).send({msg: 'Changes could not be saved'});
				}
			});
		}
	})
}


module.exports = router;
