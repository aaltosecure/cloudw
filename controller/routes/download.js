var express = require('express');
var router = express.Router();
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var Device = require('../model/device.js');
var Fs = require('fs');
var dir ='deviceFiles';
var http = require('http');
var Auth = require('./auth.js');
var logger = require("../utils/logger.js");

/**
 * Updates the field isDebugInfoRequested to true in the Device table for the device which has the id equal to devId
 */

router.get('/debug/request/:devId', function (req, res, next) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                if(err) {
                    logger.writeLog('error', null, req.user.id, err.toString());
                    res.status(500).send({msg: 'The device does not exist'});
                }
                else {
                    /**
                     * Updates the isDebugInfoRequested in the database.
                     */
                    device.updateAttribute('isDebugInfoRequested', true, function (err) {
                        if(err) {
                            logger.writeLog('error', null, req.user.id, err.toString());
                            res.status(500).send({msg: 'Cannot request debug information'});
                        }
                    });
                    res.json('');
                }
            })
        });
    });
});

/**
 * Under the deviceFiles folder, there is a folder for each registered device named after the device id.
 * Under each device's folder, there is a folder for each time the user requested the debug information, named after
 * the timestamp when the device returned the debugging information to the server. This function gets the list of the
 * files in the folder with the name devId, with the latest timestamp.
 */

router.get('/debug/filenames/:devId', function (req, res) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            var dirnames = [];
            Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'The device does not exist'});
                }
                else {
                    var pwd = dir + '/' + req.params.devId;

                    /**
                     * When the device returns the debugging information, the server sets the isDebugInfoRequested flag
                     * to false. Then the list of the filenames is sent to the user interface.
                     */
                    if (device.isDebugInfoRequested == false) {

                        /**
                         * Reads the content of the directory belonging to the device with id equal to devId.
                         */
                        Fs.readdir(pwd, function (err, files) {
                            if(err) {
                                logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'Internal Server Error'});
                            }
                            else {
                                var latest = 0;

                                /**
                                 * Finds the directory with the latest timestamp.
                                 */
                                for (var i = 0; i < files.length; i++) {
                                    if (Fs.statSync(pwd + '/' + files[i]).isDirectory()) {
                                        if (parseInt(files[i]) > latest) {
                                            latest = files[i];
                                        }
                                    }
                                }
                                /**
                                 * Send the list of the filenames to the user interface.
                                 */
                                Fs.readdir(pwd + '/' + latest, function (err, debugFiles) {
                                    if(err) {
                                        logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                                        res.status(500).send({msg: 'Internal Server Error'});
                                    }
                                    else {
                                        for (var i = 0; i < debugFiles.length; i++) {
                                            dirnames.push({filename: debugFiles[i]});
                                        }
                                        res.send(dirnames);
                                    }
                                });
                            }
                        });
                    }
                    /**
                     * Send an empty string to the user interface if the device hasn't returned yet the debugging
                     * information.
                     */
                    else {
                        res.send({filename: ''});
                    }
                }
            })
        })
    });
});


/**
 * Downloads the file that the user requested.
 */

/**
 * TODO: Test for path traversal vulnerabilities.
 */
router.get('/debug/files/:devId/:filename', function (req, res) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            var pwd = dir + '/' + req.params.devId;

            /**
             * Reads the content of the directory belonging to the device with id equal to devId.
             */
            Fs.readdir(pwd, function (err, files) {
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                }
                else {
                    var latest = 0;

                    /**
                     * Finds the directory with the latest timestamp.
                     */
                    for (var i = 0; i < files.length; i++) {
                        if (Fs.statSync(pwd + '/' + files[i]).isDirectory()) {
                            if (parseInt(files[i]) > latest) {
                                latest = files[i];
                            }
                        }
                    }

                    /**
                     * Download the file only if the file the user requested, exists in the folder.
                     */
                    Fs.readdir(pwd + '/' + latest, function (err, debugFiles) {
                        if(err) {
                            logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                            res.status(500).send({msg: 'Internal Server Error'});
                        }
                        else {
                            var correct = false;
                            for (var i = 0; i < debugFiles.length; i++) {
                                if (Fs.statSync(pwd + '/' + latest + '/' + debugFiles[i]).isFile() && debugFiles[i] == req.params.filename) {
                                    correct = true;
                                }
                            }
                            if (correct === true) {

                                /**
                                 * Send the file in the response.
                                 */
                                res.download(pwd + '/' + latest + '/' + req.params.filename);
                            }
                            else
                                res.send({msg: ''});
                        }
                    })
                }
            });
        })
    })
});




module.exports = router;