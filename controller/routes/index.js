var express = require('express');
var router = express.Router();
var passport = require('../utils/passport.js');
var Auth = require('./auth.js');
var logger = require("../utils/logger.js");

var Device = require('../model/device.js');

/* GET home page. */
router.get('/', function(req, res, next) {
	Auth.isLoggedIn(req, res, function() {
		res.render('index',
			{
				title: 'Main page',
				message: req.flash('message')
			});
	})
});

/**
 * Get Login Page
 */

router.get('/login', function(req, res, next) {
	res.render('login', 
		{
			title: 'CloudUI',
			message: req.flash('message')});
});

module.exports = router;
