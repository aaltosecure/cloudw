var express = require('express');
var router = express.Router();
var passport = require('../utils/passport.js');
var Device = require('../model/device.js');
var DeviceAlertLog = require('../model/deviceAlertLog.js');
var Auth = require('./auth.js');
var DeviceType = require('../model/deviceType.js');
var KeyMappings = require('../model/keyMappings.js');
var async = require("async");
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var ShellCommandLog = require('../model/shellCommandLog.js');
var User = require('../model/user.js');
var DeviceConfigHistory = require('../model/deviceConfigHistory.js');
var ConfigChangeSet = require('../model/configChangeSet.js');
var Schema = require('../model/schema');
var exception = 'EmptyEmptyEmpty';
var retry = 'RetryRetryRetry';
var pushRequestedState = 'PUSH_REQUESTED';
var normalState = 'NORMAL';
var failedOnlineState = 'FAILED_ONLINE';
var CurrentDeviceConfig = require('../model/deviceconfig.js');
var OwnershipChangeRequest = require('../model/ownershipChangeRequest.js');
var logger = require("../utils/logger.js");
var validWindow = 86400000;


/**
 * Get a JSON object containing the alert logs regarding the device with id equal to devId.
 */
router.get('/logs/:devId', function(req, res, next) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            DeviceAlertLog.all({where: {deviceId: req.params.devId}}, function (err, logs) {
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'Logs are not available'});
                }
                else {
                    res.json(logs);
                }
            });
        })
    })

});

/**
 * Saves the shell command that the user inserted in the database so that the server can read it and send it to
 * the device.
 */
router.post('/commands/:devId', function(req, res) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            var shellCommand = new ShellCommandLog;
            shellCommand.userId = req.user.id;
            shellCommand.deviceId = req.params.devId;
            shellCommand.shellCommand = req.body.command;
            shellCommand.save(null, function(err, savedItem) {
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'Shell command could not be executed'});
                }
            });
            res.send({error: ''})
        })
    })

});

/**
 * Get the output of the command passed as the parameter :command
 */
router.get('/output/:devId/:command', function(req, res, next) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            getOutput(req, res);
        })
    })

});

/**
 * The owner inserts the e-mail address of another user so that he can
 * give access right to the device to the temporary owner for limited time.
 */
router.post('/delegation', function(req, res, next) {
    Auth.isLoggedIn(req, res, function() {
        Auth.ownsDevice(req.body.devId, req, res, function () {
            UserDeviceMapping.all({where: {userId: req.user.id, deviceId: req.body.devId}}, function (err, devices) {
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'There is no device corresponding to this user'});
                }
                else {
                    if (devices.length === 1) {
                        User.findOne({where: {email: req.body.delegateduser}}, function (err, user) {
                            /**
                             * The temporary owner does not exist.
                             */
                            if (err) {
                                logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'Internal Server Error'});
                            }
                            else if (!user) {
                                res.send({error: 'The user ' + req.body.delegateduser + ' is not registered'})
                            }
                            else {
                                /**
                                 * The temporary owner exists.
                                 */
                                UserDeviceMapping.findOne({
                                    where: {
                                        userId: user.id,
                                        deviceId: req.body.devId,
                                        deviceAccessLevel: 'ACCESSRIGHT'
                                    }
                                }, function (err, userAccess) {
                                    if (err) {
                                        logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                                        res.status(500).send({msg: 'Internal Server Error'});
                                    }
                                    else {

                                        /**
                                         * If the owner has granted access to the temporary owner before, then just update
                                         * the expiry time.
                                         */
                                        if (userAccess != null) {
                                            var currTime = new Date().getTime();
                                            userAccess.updateAttribute('accessExpiryTime', currTime + validWindow);
                                        }

                                        /**
                                         * If the owner has not granted access to the temporary owner before, then create
                                         * a new row in the UserDeviceMapping table.
                                         */
                                        else {
                                            var newDelegatedUser = new UserDeviceMapping;
                                            newDelegatedUser.userId = user.id;
                                            newDelegatedUser.deviceId = req.body.devId;
                                            newDelegatedUser.deviceAccessLevel = 'ACCESSRIGHT';
                                            newDelegatedUser.accessExpiryTime = new Date().getTime() + validWindow;
                                            newDelegatedUser.save();
                                        }
                                        res.send({error: ''});
                                    }
                                })

                            }
                        })
                    }
                }
            })
        })
    })
});


/**
 * Get a JSON Object containing at most the 10 recent distinct timestamps in the DeviceConfigHistory table
 */
router.get('/history/:devId', function(req, res, next) {

    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            Schema.client.query("SELECT DISTINCT timestamp FROM DeviceConfigHistory WHERE deviceId=" + req.params.devId +" ORDER BY timestamp DESC LIMIT 10",
                function(err, results){
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'There was an error in reading the History Configuration'});
                }
                else {
                    res.json(results);
                }
            });
        })
    })
});

/**
 * User selects the timestamp of the previous configuration history that he wants to restore and this function
 * updates the ConfigChangeSet with the configurations in that timestamp
 */
router.post('/restoreConfig', function(req, res, next) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.body.devId, req, res, function() {
            DeviceConfigHistory.findOne({where: {timestamp: req.body.historyList, deviceId: req.body.devId}}, function (err, configHistory) {
                if(err) {
                    logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'There was an error in reading the History Configuration'});
                }
                else {
                    CurrentDeviceConfig.all({where: {deviceId: req.body.devId}}, function (err, currConfigs) {
                        if(err) {
                            logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                            res.status(500).send({msg: 'Cannot read device configuration'});
                        }
                        /**
                         * Set the state of the device to push requested.
                         */
                        setPushRequested(req.body.devId);
                        /**
                         * Parse configuration snapshot from JSON.
                         */
                        var snapshotArray = JSON.parse(configHistory.snapshot);
                        var exists;

                        Device.findOne({where: {id: req.body.devId}}, function (err, device) {
                            if (err) {
                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'The device does not exist'});
                            }
                            else {
                                DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {
                                    if (err || !device) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'This device is not supported'});
                                    }
                                    /**
                                     * Save the values in the database only if the state of the device is NORMAL,
                                     * PUSH_REQUESTED or FAILED_ONLINE
                                     */
                                    else if (device.state === normalState || device.state === pushRequestedState
                                            || device.state === failedOnlineState) {
                                        /**
                                         * The snapshotArray contains the configuration key-value pairs when the time was equal to
                                         * the timestamp (req.body.historyList). The currConfigs contains the current configuration stored
                                         * in the router. For each value in the snapshotArray, it checks if it exists in the currConfigs
                                         * array. If it does not exist, it means that the user has deleted it in the current configuration
                                         * and we need to restore it. Otherwise we need to overwrite the current configuration.
                                         */
                                        for (var i = 0; i < snapshotArray.length; i++) {
                                            exists = false;
                                            for (var j = 0; j < currConfigs.length; j++) {
                                                if (snapshotArray[i].key === currConfigs[j].key) {
                                                    exists = true;
                                                }
                                            }
                                            var newConf = new ConfigChangeSet;

                                            /**
                                             * The snapshotArray[i] key-value pair has been deleted in the current configuration and
                                             * needs to be restored.
                                             */
                                            if (exists === false) {
                                                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, changeSet) {
                                                    if (err) {
                                                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                                        res.status(500).send({msg: 'Internal Server Error'});
                                                    }
                                                    else {
                                                        pos = keyPosition(snapshotArray[i], changeSet);

                                                        /**
                                                         * If the snapshotArray[i] key-value pair does not exist in ConfigChangeSet, then create
                                                         * it.
                                                         */
                                                        if (pos === -1) {
                                                            newConf.key = snapshotArray[i].key;
                                                            newConf.deviceId = req.body.devId;
                                                            newConf.userId = req.user.id;
                                                            newConf.save();
                                                        }
                                                        /**
                                                         * If the snapshotArray[i] key-value pair exists in ConfigChangeSet, then update
                                                         * it.
                                                         */
                                                        else {
                                                            changeSet[pos].updateAttribute('value', snapshotArray[i].value, function (err) {
                                                                if (err) {
                                                                    logger.writeLog('error', req.body.devId, user.id, err.toString());
                                                                    res.status(500).send({msg: 'The configuration could not be restored'});
                                                                }
                                                            });
                                                        }
                                                    }
                                                })
                                            }
                                            /**
                                             * The snapshotArray[i] key-value pair exists in the current configuration and needs to be
                                             * overwritten.
                                             */
                                            else {
                                                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, changeSet) {
                                                    pos = keyPosition(snapshotArray[i], changeSet);
                                                    /**
                                                     * If the snapshotArray[i] key-value pair does not exist in ConfigChangeSet, then create
                                                     * it.
                                                     */
                                                    if (pos === -1) {

                                                        newConf.key = snapshotArray[i].key;
                                                        newConf.value = snapshotArray[i].value;
                                                        newConf.deviceId = req.body.devId;
                                                        newConf.userId = req.user.id;
                                                        newConf.save(null, function (err, savedItem) {
                                                            if (err) {
                                                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                                                res.status(500).send({msg: 'The configuration could not be restored'});
                                                            }
                                                        });
                                                    }
                                                    /**
                                                     * If the snapshot[i] key-value pair exists in ConfigChangeSet, then update
                                                     * it.
                                                     */
                                                    else {
                                                        changeSet[pos].updateAttribute('value', snapshotArray[i].value, function (err) {
                                                            if (err) {
                                                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                                                res.status(500).send({msg: 'The configuration could not be restored'});
                                                            }
                                                        });
                                                    }
                                                })
                                            }
                                        }
                                        res.send({error: ''});
                                    }
                                    else {
                                        res.json({error: 'Changes were not saved because the device is locked'});
                                    }
                                })
                            }
                        })
                    });
                }
            })

        })
    })
});

/**
 * In some cases, when the user wants to sell the device for example, he needs to change the owner of the device
 * in the software.
 */
router.post('/changeownership', function (req, res) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.body.devId, req, res, function() {

            /**
             * The owner decided to share the history configuration with the new owner.
             */
            if(req.body.sharehistory == 'yes') {
                OwnershipChangeRequest.findOne({where: {oldOwner_id: req.user.id, old_DeviceId: req.body.devId}}, function (err, request) {
                    if(err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'Internal Server Error'});
                    }
                    else {

                        /**
                         * If there is no previous ownership request, then create a new entry in the table.
                         */
                        if (request == null) {
                            var ownershipRequest = new OwnershipChangeRequest;
                            ownershipRequest.isInfoShared = 1;
                            ownershipRequest.old_DeviceId = req.body.devId;
                            ownershipRequest.oldOwner_Id = req.user.id;
                            ownershipRequest.newOwner_Email = req.body.newowner;
                            ownershipRequest.isHandled = 0;
                            ownershipRequest.save(null, function (err, savedItem) {
                                if(err) {
                                    logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                    res.status(500).send({msg: 'Ownership could not be changed'});
                                }
                            });
                            res.send({error: ''});
                        }
                        /**
                         * If there is a previous ownership request then return error.
                         */
                        else {
                            res.send({error: 'Email exists'});
                        }
                    }
                });
            }

            /**
             * The owner decided not to share the history configuration with the new owner.
             */
            else if (req.body.sharehistory == 'no') {
                OwnershipChangeRequest.findOne({where: {oldOwner_id: req.user.id, old_DeviceId: req.body.devId}}, function (err, request) {
                    if(err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'Ownership could not be changed'});
                    }
                    else {

                        /**
                         * If there is no previous ownership requests, then create a new entry in the table.
                         */
                        if (request == null) {
                            var ownershipRequest = new OwnershipChangeRequest;
                            ownershipRequest.isInfoShared = 0;
                            ownershipRequest.old_DeviceId = req.body.devId;
                            ownershipRequest.newOwner_Email = req.body.newowner;
                            ownershipRequest.oldOwner_Id = req.user.id;
                            ownershipRequest.isHandled = 0;
                            ownershipRequest.save(null, function (err, savedItem) {
                                if(err) {
                                    logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                    res.status(500).send({msg: 'Ownership could not be changed'});
                                }
                            });
                            res.send({error: ''});
                        }
                        /**
                         * If there is a previous ownership request then return error.
                         */
                        else {
                            res.send({error: 'Email exists'});
                        }
                    }
                });
            }
        })
    })
});

/**
 * Returns the index in the changeSet array where config is found. Otherwiser return -1.
 * @param config
 * @param changeSet
 * @returns {*}
 */
function keyPosition(config, changeSet){
    for(var c in changeSet)
        if(changeSet.hasOwnProperty(c))
            if (changeSet[c].key === config.key)
                return c;
    return -1;
}

/**
 * Get a JSON object containing the output of the executed shell command or if the output is not available yet, it
 * returns  retry. There might be different rows in the table for the same shell command. The function returns the latest
 * executed shell command.
 * @param req
 * @param res
 */
function getOutput (req, res) {
    ShellCommandLog.all({where: {deviceId: req.params.devId, userId: req.user.id, shellCommand: req.params.command}, order: 'id DESC'}, function (err, output) {
        if(err) {
            logger.writeLog('error', req.params.devId, req.user.id, err.toString());
            res.json({retry: false,
                      output: 'Error',
                      result: 'There was an error in executing the shell command. Please try again.'
            })
        }
        else {
            if (output[0].output != null) {
                res.json({
                    retry: false,
                    output: output[0].output,
                    result: 'The command was successfully executed'
                });
            }
            else {
                res.json({retry: true})
            }
        }
    });
}

/**
 * Sets the state of the device with id equal to deviceId, to PUSH_REQUESTED.
 * @param deviceId
 */
function setPushRequested(deviceId) {
    Device.findOne({where: {id: deviceId}}, function (err, device) {
        if(err) {
            logger.writeLog('error', req.body.devId, req.user.id, err.toString());
            res.status(500).send({msg: 'Device does not exist'});
        }
        else {
            device.updateAttribute('state', pushRequestedState, function (err) {
                if(err) {
                    logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'Device state could not be changed'});
                }
            });
        }
    })
}

module.exports = router;