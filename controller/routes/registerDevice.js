var express = require('express');
var router = express.Router();
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

var Device = require('../model/device.js');
var logger = require("../utils/logger.js");
var UserDeviceMapping = require('../model/userDeviceMapping.js');

// Options for passport authentication
var ppOptions = {failureRedirect: '/registerDevice', failureFlash: true}; 


// Login with an existing account and pair the device to that
router.post('/login', isUnclaimedDevice, 
	passport.authenticate('local-login', ppOptions), 
	pairUserAndDevice, function(req, res) {
	
	res.redirect("/");
});

// Create a new account and pair the device to that
router.post('/createAccount', isUnclaimedDevice, 
	passport.authenticate('local-signup', ppOptions),
	pairUserAndDevice, function(req, res) {
		req.logout();
	res.redirect('../../login');
});

// Display login/register site for the new device/user
router.get('/', isUnclaimedDevice, function(req, res, next) {
	res.render('registerDevice', 
		{ title: "Hello new gateway user!", 
		  message: req.flash('message')});
});


/**
 * This does not actually create the device but 
 * assumes it already exists and has not been yet paired.
 */
function pairUserAndDevice(req, res, next) {
	Device.findOne({where: {key: req.deviceFingerprint}}, function(err, device) {
		if(err) {
			logger.writeLog('error', null, req.user.id, err.toString());
			res.status(500).send({msg: 'The device does not exist'});
		}
		else {
			var newMapping = new UserDeviceMapping;
			newMapping.userId = req.user.id;
			newMapping.deviceId = device.id;
			newMapping.deviceAccessLevel = 'OWNERSHIP';
			newMapping.save();
			next();
		}
	});
}

function isUnclaimedDevice(req, res, next) {
	// Is connection coming through a device
	if(!req.deviceFingerprint) {
		res.send("Connection is not through the gateway!");
		return;
	}

	// Is connection coming through an *unregistered* device
	Device.findOne({where: {key: req.deviceFingerprint}}, function(err, device) {
		if(err) {
			logger.writeLog('error', null, req.user.id, err.toString());
			res.status(500).send({msg: 'The device does not exist'});
		} else if(!device) {
			res.send("The device has either already been paired with a user or the daemon on it is not running.");
		} else {
			next();
		}
	});
}





module.exports = router;
