var express = require('express');
var router = express.Router();
var async = require("async");
var schema = require("../model/schema");
var User = require('../model/user.js');
var passport = require('../utils/passport.js');

var Device = require('../model/device.js');
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var ConfigChangeSet = require('../model/configChangeSet.js');
var KeyMappings = require('../model/keyMappings.js');
var DeviceType = require('../model/deviceType.js');
var Auth = require('./auth.js');
var pushRequestedState = 'PUSH_REQUESTED';
var normalState = 'NORMAL';
var Validate = require('./validate.js');
var logger = require("../utils/logger.js");

/**
 * Saves user input coming from the Easy Tab, in the database. For each input field in the user interface,
 * the function calls savevalues() function.
 */
router.post('/easy',  function(req, res) {
    Auth.isLoggedIn(req, res, function () {
        Auth.ownsDevice(req.body.devId, req, res, function () {
            /**
             * Check if the user input is validated.
             */
            if (Validate.isEasyValidated(req)) {
                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, configs) {
                    if (err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'The device configuration is not available'});
                    }
                    else {
                        Device.findOne({where: {id: req.body.devId}}, function (err, device) {
                            if (err) {
                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'The device does not exist'});
                            }
                            else {
                                DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {

                                    if (err || !device) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'This device is not supported'});
                                    }
                                    /**
                                     * Save the values in the database only if the state of the device is NORMAL or
                                     * PUSH_REQUESTED
                                     */
                                    else if (device.state === normalState || device.state === pushRequestedState) {
                                        /**
                                         * Save the values from the user input in the database. Set device's state to PUSH_REQUESTED.
                                         * In addition to the wireless password, some other parameters are required depending
                                         * on the device model in order to correctly enable wireless. For example enablewlan
                                         * is required for Open-WRT but should not be inserted for DD-WRT.
                                         * TODO: Insert all the values depending on the device model, in order to correctly
                                         * TODO: enable wireless.
                                         */
                                        saveValues('wirelessname', req.body.wirelessname, type, configs, req);
                                        saveValues('wpawirelesspw', req.body.wpawirelesspw, type, configs, req);
                                        saveValues('wlnetmode', 'mixed', type, configs, req);
                                        saveValues('wlpasswtype', 'psk', type, configs, req);
                                        saveValues('enablewlan', '0', type, configs, req);
                                        setPushRequested(req.body.devId);
                                        res.json({error: ''});
                                    }
                                    else {
                                        res.json({error: 'Changes were not saved because the device is locked'});
                                    }
                                })
                            }
                        })
                    }
                })
            }
            /**
             * If user's input is not valid, then send an error.
             */
            else {
                res.json({error: 'Input not correct'});
            }
        })
    });
});

/**
 * Saves user input coming from the Normal Tab, in the database. For each input field in the user interface,
 * the function calls savevalues() function.
 */
router.post('/normal', function(req, res, next) {
    Auth.isLoggedIn(req, res, function() {
        Auth.ownsDevice(req.body.devId, req, res, function () {
            /**
             * Check if the user input is validated.
             */
            if (Validate.isNormalValidated(req)) {
                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, configs) {
                    if(err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'The device configuration is not available'});
                    }
                    else {
                        Device.findOne({where: {id: req.body.devId}}, function (err, device) {
                            if (err) {
                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'The device does not exist'});
                            }
                            else {
                                DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {
                                    if (err || !device) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'This device is not supported'});
                                    }
                                    else if (device.state === normalState || device.state === pushRequestedState) {

                                        /**
                                         * Save the values from the user input in the database. Set device's state to PUSH_REQUESTED.
                                         * In addition to the wireless password, some other parameters are required depending
                                         * on the device model in order to correctly enable wireless. For example enablewlan
                                         * is required for Open-WRT but should not be inserted for DD-WRT.
                                         * TODO: Insert all the values depending on the device model, in order to correctly
                                         * TODO: enable functionality.
                                         */
                                        saveValues('hostname', req.body.hostname, type, configs, req);
                                        saveValues('localip', req.body.localip, type, configs, req);
                                        saveValues('subnetmask', req.body.subnetmask, type, configs, req);
                                        saveValues('defaultgw', req.body.defaultgw, type, configs, req);
                                        saveValues('localdns', req.body.localdns, type, configs, req);
                                        saveValues('wirelessname', req.body.wirelessname, type, configs, req);
                                        saveValues('wlpasstype', req.body.wlpasstype, type, configs, req);
                                        saveValues('wlnetmode', req.body.wlnetmode, type, configs, req);
                                        saveValues('ap', req.body.ap, type, configs, req);
                                        saveValues('ssidbcast', req.body.ssidbcast, type, configs, req);

                                        /**
                                         * Depending on the wireless password type, the key for the wireless
                                         * password will change.
                                         */
                                        switch (req.body.wlpasstype) {
                                            case 'none':
                                                saveValues('nowirelesspw', 'No wireless password', type, configs, req);
                                                break;
                                            case 'wep':
                                                saveValues('wepwirelesspw', req.body.wirelesspw, type, configs, req);
                                                break;
                                            case 'wpa':
                                                saveValues('wpawirelesspw', req.body.wirelesspw, type, configs, req);
                                                break;
                                            default:
                                                break;
                                        }

                                        setPushRequested(req.body.devId);
                                        res.send({error: ''});
                                    }
                                    else {
                                        res.json({error: 'Changes were not saved because the device is locked'});
                                    }
                                })
                            }
                        })
                    }
                });
            }
            /**
             * If user's input is not valid, then send an error.
             */
            else {
                res.send({error: 'Input not correct'});
            }
        })
    })
});


router.post('/advanced/network', function(req, res, next) {
    Auth.isLoggedIn(req, res, function () {
        Auth.ownsDevice(req.body.devId, req, res, function () {
            /**
             * Check if the user input is validated.
             */
            /**
             * TODO: Create a new function to validate the user input in the Advanced/Network Tab
             */
            if (Validate.isEasyValidated(req)) {
                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, configs) {
                    if (err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'The device configuration is not available'});
                    }
                    else {
                        Device.findOne({where: {id: req.body.devId}}, function (err, device) {
                            if (err) {
                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'The device does not exist'});
                            }
                            else {
                                DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {

                                    if (err || !device) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'This device is not supported'});
                                    }
                                    /**
                                     * Save the values in the database only if the state of the device is NORMAL or
                                     * PUSH_REQUESTED
                                     */
                                    else if (device.state === normalState || device.state === pushRequestedState) {
                                        /**
                                         * Save the values from the user input in the database. Set device's state to PUSH_REQUESTED.
                                         * In addition to the wireless password, some other parameters are required depending
                                         * on the device model in order to correctly enable wireless. For example enablewlan
                                         * is required for Open-WRT but should not be inserted for DD-WRT.
                                         * TODO: Insert all the values depending on the device model, in order to correctly
                                         * TODO: enable wireless.
                                         */

                                        /**
                                         * TODO: Save the values from the user input in the Advanced/Network Tab to the database
                                         * TODO: by applying the savevalues() function.
                                         */
                                        setPushRequested(req.body.devId);
                                        res.json({error: ''});
                                    }
                                    else {
                                        res.json({error: 'Changes were not saved because the device is locked'});
                                    }
                                })
                            }
                        })
                    }
                })
            }
            /**
             * If user's input is not valid, then send an error.
             */
            else {
                res.json({error: 'Input not correct'});
            }
        })
    });
});

router.post('/advanced/wireless', function(req, res, next) {
    Auth.isLoggedIn(req, res, function () {
        Auth.ownsDevice(req.body.devId, req, res, function () {
            /**
             * Check if the user input is validated.
             */
            /**
             * TODO: Create a new function to validate the user input in the Advanced/Wireless Tab
             */
            if (Validate.isEasyValidated(req)) {
                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, configs) {
                    if (err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'The device configuration is not available'});
                    }
                    else {
                        Device.findOne({where: {id: req.body.devId}}, function (err, device) {
                            if (err) {
                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'The device does not exist'});
                            }
                            else {
                                DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {

                                    if (err || !device) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'This device is not supported'});
                                    }
                                    /**
                                     * Save the values in the database only if the state of the device is NORMAL or
                                     * PUSH_REQUESTED
                                     */
                                    else if (device.state === normalState || device.state === pushRequestedState) {
                                        /**
                                         * Save the values from the user input in the database. Set device's state to PUSH_REQUESTED.
                                         * In addition to the wireless password, some other parameters are required depending
                                         * on the device model in order to correctly enable wireless. For example enablewlan
                                         * is required for Open-WRT but should not be inserted for DD-WRT.
                                         * TODO: Insert all the values depending on the device model, in order to correctly
                                         * TODO: enable wireless.

                                         /**
                                         * TODO: Save the values from the user input in the Advanced/Network Tab to the database
                                         * TODO: by applying the savevalues() function.
                                         */
                                        setPushRequested(req.body.devId);
                                        res.json({error: ''});
                                    }
                                    else {
                                        res.json({error: 'Changes were not saved because the device is locked'});
                                    }
                                })
                            }
                        })
                    }
                })
            }
            /**
             * If user's input is not valid, then send an error.
             */
            else {
                res.json({error: 'Input not correct'});
            }
        })
    });
});

router.post('/advanced/services', function(req, res, next) {
    Auth.isLoggedIn(req, res, function () {
        Auth.ownsDevice(req.body.devId, req, res, function () {
            /**
             * Check if the user input is validated.
             */
            /**
             * TODO: Create a new function to validate the user input in the Advanced/Services Tab
             */
            if (Validate.isEasyValidated(req)) {
                ConfigChangeSet.all({where: {deviceId: req.body.devId}}, function (err, configs) {
                    if (err) {
                        logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                        res.status(500).send({msg: 'The device configuration is not available'});
                    }
                    else {
                        Device.findOne({where: {id: req.body.devId}}, function (err, device) {
                            if (err) {
                                logger.writeLog('error', req.body.devId, req.user.id, err.toString());
                                res.status(500).send({msg: 'The device does not exist'});
                            }
                            else {
                                DeviceType.findOne({where: {id: device.deviceTypeId}}, function (err, type) {

                                    if (err || !device) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'This device is not supported'});
                                    }
                                    /**
                                     * Save the values in the database only if the state of the device is NORMAL or
                                     * PUSH_REQUESTED
                                     */
                                    else if (device.state === normalState || device.state === pushRequestedState) {
                                        /**
                                         * Save the values from the user input in the database. Set device's state to PUSH_REQUESTED.
                                         * In addition to the wireless password, some other parameters are required depending
                                         * on the device model in order to correctly enable wireless. For example enablewlan
                                         * is required for Open-WRT but should not be inserted for DD-WRT.
                                         * TODO: Insert all the values depending on the device model, in order to correctly
                                         * TODO: enable wireless.

                                         /**
                                         * TODO: Save the values from the user input in the Advanced/Network Tab to the database
                                         * TODO: by applying the savevalues() function.
                                         */
                                        setPushRequested(req.body.devId);
                                        res.json({error: ''});
                                    }
                                    else {
                                        res.json({error: 'Changes were not saved because the device is locked'});
                                    }
                                })
                            }
                        })
                    }
                })
            }
            /**
             * If user's input is not valid, then send an error.
             */
            else {
                res.json({error: 'Input not correct'});
            }
        })
    });
});

/**
 * Get a boolean which is false when the device is in normal state. True otherwise.
 */
router.get('/state/check/:devId', function(req, res, next) {
    Auth.isLoggedIn(req, res, function(){
        Auth.ownsDevice(req.params.devId, req, res, function() {
            Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                if(err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'The device does not exist'});
                }
                else {
                    if (device.state == normalState) {
                        res.send({retry: false});
                    }
                    else {
                        res.send({retry: true});
                    }
                }
            })
        })
   })
});


/**
 * Add a new configuration key-value pair in the database for the device with id equal to devId.
 */

router.post('/:devId/addConfig', function(req, res, next) {
    // TODO input validation (for the model)
    Auth.isLoggedIn(req, res, function () {
        Auth.ownsDevice(req.params.devId, req, res, function () {
            ConfigChangeSet.all({where: {deviceId: req.params.devId}}, function (err, configs) {
                if (err) {
                    logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                    res.status(500).send({msg: 'The device configuration is not available'});
                }
                else {

                    Device.findOne({where: {id: req.params.devId}}, function (err, device) {
                        if (err || !device) {
                            logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                            res.status(500).send({msg: 'The device does not exist'});
                        }
                        else if (device.state === normalState || device.state === pushRequestedState) {
                            /**
                             * Save the values from the user input in the database. Set device's state to PUSH_REQUESTED
                             */
                            setPushRequested(device.id);
                            var pos = keyPosition(configs, req.body.key);
                            /**
                             * If the inserted key does not exist in the ConfigChangeSet, then create a new entry.
                             * Otherwise update the existing entry.
                             */
                            if (pos === -1) {
                                var newConf = new ConfigChangeSet;
                                newConf.key = req.body.key;
                                newConf.value = req.body.value;
                                newConf.deviceId = req.params.devId;
                                newConf.userId = req.user.id;
                                newConf.save(null, function (err) {
                                    if (err) {
                                        logger.writeLog('error', req.params.devId, req.user.id, err.toString());
                                        res.status(500).send({msg: 'The configuration could not be inserted'});
                                    }
                                });
                            }
                            else {
                                configs[pos].updateAttribute('value', req.body.value, function (err) {
                                    if (err) {
                                        logger.writeLog('error', null, null, err.toString());
                                        res.status(500).send({msg: 'The configuration could not be inserted'});
                                    }
                                });
                            }
                            res.redirect('../../');
                        }
                        else {
                            res.redirect('../../');
                            // res.json({error: 'Changes were not saved because the device is locked'});
                        }
                    })
                }
            })
        })
    })
});

/**
 * Saves user input in the ConfigChangeSet table. For each key-value pair, if an entry exists in the ConfigChangeSet,
 * then updates the value field. Otherwise it create a new entry in the table.
 * @param inputField
 * @param bodyRequest
 * @param type
 * @param configs
 */
function saveValues(inputField, bodyRequest, type, configs, req){
    KeyMappings.findOne({where: {inputName: inputField, deviceTypeId: type.id}}, function (err, keyMap) {
        if (err) {
            logger.writeLog('error', null, null, err.toString());
            res.status(500).send({msg: 'Internal Server Error'});
        }
        else {

            /**
             * Considering that the software supports different versions of routers, the KeyMappings table contains the keys
             * to be modified for each particular user input field and for particular hardware and software type of
             * the router. The KeyMappings should be inserted manually in the table for each router that the software
             * supports.
             * @type {*}
             */
            var pos = keyPosition(configs, keyMap.key);
            if (pos === -1) {
                var newConf = new ConfigChangeSet;
                newConf.key = keyMap.key;
                newConf.value = bodyRequest;
                newConf.deviceId = req.body.devId;
                newConf.userId = req.user.id;
                newConf.save(null, function (err) {
                    logger.writeLog('error', null, null, err.toString());
                    res.status(500).send({msg: 'The configuration could not be inserted'});
                });
            }
            else {
                configs[pos].updateAttribute('value', bodyRequest, function (err) {
                    if (err) {
                        logger.writeLog('error', null, null, err.toString());
                        res.status(500).send({msg: 'The configuration could not be inserted'});
                    }
                });
            }
        }
    });
}


/**
 * Sets the state of the device with id equal to deviceId, to PUSH_REQUESTED.
 * @param deviceId
 */
function setPushRequested(deviceId) {
    Device.findOne({where: {id: deviceId}}, function (err, device) {
        if(err) {
            logger.writeLog('error', deviceId, null, err.toString());
            res.status(500).send({msg: 'The device does not exist'});
        }
        else {
            device.updateAttribute('state', pushRequestedState, function(err) {
                if(err) {
                    logger.writeLog('error', deviceId, null, err.toString());
                    res.status(500).send({msg: 'The device state was not updated'});
                }
            });
        }
    })
}

/**
 * Returns the index in the config array where key is found. Otherwise return -1.
 */
function keyPosition(configs, key){
    for(var c in configs) {
        if (configs[c].key === key)
            return c;
    }
    return -1;
}

module.exports = router;