var logger = require("../utils/logger.js");

/**
 *  This function checks if the user input in the Easy Tab is validated or not.
 */

function isEasyValidated(req) {
    var isValidated = true;

    // Checks if the user input wirelessname and wpawirelesspw is composed of alpha-numeric characters
    /**
     * TODO: Validate user input according to the wireless standards. For example the SSID can contain the character '-'
     */
    if(! isAlphaNumeric(req.body.wirelessname) || ! isAlphaNumeric(req.body.wpawirelesspw))
        isValidated = false;

    // Checks the length of wirelessname and wpawirelesspw
    if(req.body.wirelessname.length > 32 || req.body.wpawirelesspw.length < 8 || req.body.wpawirelesspw.length > 64)
        isValidated = false;
    logger.writeLog('info', null, req.user.id, 'IsEasyValidated function returns ' + isValidated);

    return isValidated;
}

/**
 * This function checks if the user input in the Normal Tab is validated or not.
 */

function isNormalValidated(req) {
    var isValidated = true;


    // Checks if the user input is composed of alpha-numeric characters
    if(! isAlphaNumeric(req.body.routername) || ! isAlphaNumeric(req.body.wirelessname)
        || ! isAlphaNumeric(req.body.wirelesspw)  || ! isAlphaNumeric(req.body.wlpasswtype)  || ! isAlphaNumeric(req.body.wlnetmode)
        || ! isAlphaNumeric(req.body.ap) || ! isAlphaNumeric(req.body.ssidbcast))
        isValidated = false;

    // Checks the length of wirelessname
    if(req.body.wirelessname.length > 32)
        isValidated = false;

    // If user is using WPA, check the password length
    if( req.body.wpawirelesspw) {
        if (req.body.wpawirelesspw.length < 8 || req.body.wpawirelesspw.length > 64)
            isValidated = false;
    }

    // Checks if the user input is a valid IP address
    if(! isValidIpAddress(req.body.localip) || ! isValidIpAddress(req.body.subnetmask) || ! isValidIpAddress(req.body.defaultgw)
        || ! isValidIpAddress(req.body.localdns))
        isValidated = false;

    logger.writeLog('info', null, req.user.id, 'IsNormalValidated function returns ' + isValidated);
    return isValidated;
}

/**
 *   Checks if the string is composed of alpha-numeric characters or not.
 */

function isAlphaNumeric(string) {
    return /[a-zA-Z0-9]/.test(string);
}

/**
 *   Checks if string is a valid IP Address.
 */

function isValidIpAddress(string) {
    var octets = string.split('.');
    if(octets.length === 4) {
        for(var o in octets) {
            if(parseInt(octets[o]) < 0 || parseInt(octets[o]) > 255)
                return false;
        }
    }
    return true;
}

/**
 * TODO: Validate user input in the other tabs too.
 */

module.exports.isEasyValidated = isEasyValidated;
module.exports.isNormalValidated = isNormalValidated;