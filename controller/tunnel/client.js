var net = require('net');
var sys = require('sys');

var msg = "GET /\n\n";

client = net.createConnection(8080, function() {
    console.log("Sending data: " + msg);
    client.write(msg);
});

client.addListener("data", function (data) {
    console.log("Received: " + data);
	client.destroy();
});

