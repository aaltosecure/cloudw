var tls = require('tls');
var fs = require('fs');
var sys = require('sys');
var http = require('http');
var net = require('net');
var util = require("util");
var DeviceConnection = require("../model/deviceConnection");
 
var key = fs.readFileSync('./certs/server-key.pem');
var cert = fs.readFileSync('./certs/server-cert.pem');
var ca = fs.readFileSync('./certs/deviceRootCA.pem');

/* Local server for testing purposes */
/*var server = http.createServer(function (req, res) {
	console.log("Connection to HTTP server from port: " + req.socket.remotePort);

	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end("Hello world");
});
server.listen(8080);*/



/* TUNNEL END-POINT */

console.log("TLS server started.");
var options = {
    key: key,
    cert: cert,
	ca: [ca],
	requestCert: true
};
 
var tunnelEndPoint = tls.createServer(options, function (socket) {
	console.log("TLS connection established. Peer cert fingerprint: "
		+ socket.getPeerCertificate().fingerprint);
	/* TODO Currently the process is following after the tunnel endpoint:
	 * 1. Create socket to the HTTP(s) server
	 * 2. Get localPort of the socket
	 * 3. Add pair <certificateFingerprint, localPort> to database
	 * 
	 * This may lead to some problems. Instead the database entry should be
	 * added to DB before creating the socket. This requires binding the 
	 * socket to certain localPort. This feature is not yet supported in node
	 * and should be implemented once version 0.11.13 is released.
	 * 
	 * https://github.com/joyent/node/issues/7688
	*/
	client = net.createConnection({port: 443/*, localPort: 6666*/}, function() {
		console.log("Connecting the HTTP server from port " + this.localPort);

		var deviceConn = new DeviceConnection;
		deviceConn.key = socket.getPeerCertificate().fingerprint;
		deviceConn.port = this.localPort;
		deviceConn.save();
	});

	/*client.addListener("data", function(data) {
		console.log("Data: " + data);
	});*/

	/*socket.addListener("data", function(data) {
		console.log("Data: " + data);
		socket.write(".");
	});*/

	this.addListener("close", function() {
		client.destroy();
	});

	client.fd = socket.fd;
	client.pipe(socket).on('error', function(e){ console.log(e); });
	socket.pipe(client).on('error', function(e){ console.log(e); });
});

tunnelEndPoint.listen(8081);


