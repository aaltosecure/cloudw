var tls = require('tls');
var fs = require('fs');
var sys = require('sys');
var net = require('net');
 
var options = {
	key: fs.readFileSync('./certs/switch.key'),
	cert: fs.readFileSync('./certs/switch.crt'),
	ca: [ fs.readFileSync('./certs/server-cert.pem') ],
	rejectUnauthorized: false	// TODO remove this
};
 
console.log("Tunnel started.");
 
var server = net.createServer(function (socket) {

	tunnelConnection = tls.connect(8081, options, function() {
		// TODO error handling
		console.log("Connection from " + socket.remoteAddress);
	});

	socket.addListener("close", function () {
		//close the tunnel when the client finishes the connection.
		tunnelConnection.destroy();
	});
	
	tunnelConnection.fd = socket.fd;
	tunnelConnection.pipe(socket);
	socket.pipe(tunnelConnection);

	/* Listeners for both connections. Might be useful for debugging... */
	/*socket.addListener("data", function (data) {
		console.log("Data received from client: " + data);
	});*/

});
 
server.listen(8080);
