var winston = require('winston');
winston.emitErrs = true;

var logger = new winston.Logger(
    {
        transports: [
            new winston.transports.File(
                {
                    level: 'debug',//levels: debug, info, warn, error, etc.
                    name: 'application_debug',
                    filename: 'logs/debug.log',//Log file name
                    handleExceptions: true,
                    json: true,
                    maxsize: 5242880, //5MB
                    maxFiles: 10,
                    colorize: true
                }
            ),
            new (winston.transports.Console)(
                {
                    level: 'debug',
                    colorize: 'all'
                }
            )
        ],
        exitOnError: false
    }
);

function writeLog(logLevel, deviceId, userId, message) {
    //console.log(logLevel + ' : ' + message);
    logger.log(logLevel, message, { deviceId : deviceId, userId : userId });
}

module.exports = logger;
module.exports.writeLog = writeLog;