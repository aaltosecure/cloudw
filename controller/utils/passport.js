var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var User = require('../model/user.js');
var FacebookStrategy = require('passport-facebook');

/**
 * Facebook API key, secret and callback_URL taken from the Facebook account which created the Facebook Application
 * The developer mode is enabled in the Facebook account
 * @type {string}
 */
var facebook_api_key = "698262730307235";
var facebook_api_secret = "a3fe98cd78c35bf9e96bc5db88abc633";
var callback_url = "https://localhost/auth/facebook/callback";

var OwnershipChangeRequest = require('../model/ownershipChangeRequest.js');
var UserDeviceMapping = require('../model/userDeviceMapping.js');
var Device = require('../model/device.js');
var Auth = require('../routes/auth.js');
var RESETSTAGE2 = 'RESET-STAGE2';
var logger = require("../utils/logger.js");




var isValidPassword = function(password, hash){
	return bCrypt.compareSync(password, hash);
};

var createHash = function(password){
	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};


module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, {userid: user.id, username: user.email});
    });

    // used to deserialize the user
    passport.deserializeUser(function(serializedUser, done) {
        User.find(serializedUser.userid, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) {
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

			User.findOne({where: {'email': username}}, function(err, user) {
                logger.writeLog('info', null, null, 'User signed up ' + username );
				if (err) {
                    logger.writeLog('error', null, null, err.toString());
                    return done(err);
                }

				// check if username already taken
				if(user) {
					return done(null, false, req.flash('message', 'Email is already taken'));
				}

                /**
                 * Check if the password conforms the password policy
                 */
                if(!Auth.passwordPolicy(password)) {
                    logger.writeLog('warn', null, null, 'User ' + username + ' inserted a wrong password') ;
                    return done(null, false, req.flash('message', 'Password should be more than 8 characters long'));
                }

                if(!Auth.emailPolicy(username)) {
                    logger.writeLog('warn', null, null, 'User ' + username + ' inserted a wrong email') ;
                    return done(null, false, req.flash('message', 'Password should be more than 8 characters long'));
                }

				var newUser = new User();
				newUser.email = username;
				newUser.password = createHash(password);
				newUser.save(function(err, savedItem) {
					if(err) {
                        logger.writeLog('error', null, savedItem.id, err.toString());
                        throw err;
                    }
					return done(null, newUser);
				});
			});
		});
	}));


	

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
		User.findOne({where: {'email': username}}, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err) {
                logger.writeLog('error', null, user.id, err.toString());
                return done(err);
            }

            // if no user is found, return the message
            if (!user) 
                return done(null, false, req.flash('message', 'Invalid email or password'));

            // if the user is found but the password is wrong
            if (!isValidPassword(password, user.password)) {
                logger.writeLog('error', null, user.id, 'Invalid email or password') ;
                return done(null, false, req.flash('message', 'Invalid email or password'));
            }
            ownershipHandover(req, user);
            // all is well, return successful user
            return done(null, user);
        });

    }));

    /**
     * Use Facebook strategy for Facebook login.
     */
    passport.use(new FacebookStrategy({
            clientID: facebook_api_key,
            clientSecret: facebook_api_secret,
            callbackURL: callback_url,
            enableProof: false
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                logger.writeLog('info', null, profile.id, 'User ' + profile.id + ' successfully logged in with Facebook') ;
                return done(null, profile);
            })
        }
    ));
};

/**
 * This function manages the cases when the old owner of a device changes the ownership of the device to the
 * user currently trying to log in.
 * @param req
 * @param user
 */
function ownershipHandover(req, user) {
    /**
     * Find all ownership change requests for the user currently trying to log in.
     */
    OwnershipChangeRequest.all({where: {isHandled: 0, newOwner_Email: user.email}}, function (err, requests) {
        if (err) {
            logger.writeLog('error', null, user.id, err.toString());
        }
        else if (requests.length > 0) {
            for (var r in requests) {
                UserDeviceMapping.findOne({
                    where: {
                        deviceId: requests[r].old_DeviceId,
                        deviceAccessLevel: 'OWNERSHIP'
                    }
                }, function (err, mapping) {
                    if (err) {
                        logger.writeLog('error', null, user.id, err.toString());
                        res.status(500).send({msg: 'Internal Server Error'});
                    }
                    else {
                        /**
                         * The requests array contain information about the new routers of the new owner.
                         * Mapping contains the mapping of the old owner dhe device whose ownership is going to change.
                         */

                        /**
                         * If the old owner has decided to share the configuration history with the new owner, then
                         * assign the new user id to the mapping and change isHandled.
                         */
                        if (requests[r].isInfoShared == 1) {
                            requests[r].isHandled = 1;
                            requests[r].save(null, function (err, savedItem) {
                                if(err) {
                                    logger.writeLog('error', null, user.id, err.toString());
                                }
                            });
                            mapping.userId = user.id;
                            mapping.save(null, function (err, savedItem) {
                                if(err) {
                                    logger.writeLog('error', null, user.id, err.toString());
                                }
                            });
                        }
                        /**
                         * If the old owner has decided not to share the configuration history with the new owner, then
                         * create a new device in the state RESET-STAGE2 and a new UserDeviceMapping with the new owner
                         * id and the new device id.
                         */
                        else {
                            Device.findOne({where: {id: requests[r].old_DeviceId}}, function (err, device) {
                                if (err) {
                                    logger.writeLog('error', null, user.id, err.toString());
                                    res.status(500).send({msg: 'Internal Server Error'});
                                }
                                else {
                                    var newDevice = new Device;
                                    newDevice.key = device.key;
                                    newDevice.allowOffband = device.allowOffband;
                                    newDevice.deviceTypeId = device.deviceTypeId;
                                    newDevice.state = RESETSTAGE2;
                                    newDevice.lastLockTime = device.lastLockTime;
                                    newDevice.isDebugInfoRequested = device.isDebugInfoRequested;
                                    newDevice.save(null, function (err, savedDevice) {
                                        if (err) {
                                            logger.writeLog('error', null, user.id, err.toString());
                                            res.status(500).send({msg: 'Internal Server Error'});
                                        }
                                        else {
                                            device.key = null;
                                            device.save();
                                            var newMapping = new UserDeviceMapping;
                                            newMapping.userId = user.id;
                                            newMapping.deviceId = savedDevice.id;
                                            newMapping.deviceAccessLevel = 'OWNERSHIP';
                                            newMapping.save();
                                            requests[r].isHandled = 1;
                                            requests[r].new_DeviceId = savedDevice.id;
                                            requests[r].save(null, function (err, savedItem) {
                                                if(err)
                                                    logger.writeLog('error', null, user.id, err.toString());
                                            });
                                        }
                                    });
                                }
                                // device.saveDevicehistory
                            });

                            mapping.userId = user.id;
                            mapping.save(null, function (err, savedItem) {
                                if(err) {
                                    logger.writeLog('error', null, user.id, err.toString());
                                    res.status(500).send({msg: 'Internal Server Error'});
                                }
                            });
                        }

                    }
                })
            }
        }
        /**
         * When the old owner did not change the ownership of the device but the new owner resets the device.
         */
        else {
            Device.findOne({where: {state: 'RESET-STAGE1', key: req.deviceFingerprint}}, function (err, device) {
                if (!err && device != null) {
                    UserDeviceMapping.findOne({
                        where: {
                            deviceId: device.id,
                            userId: user.id,
                            deviceAccessLevel: 'OWNERSHIP'
                        }
                    }, function (err, mapping) {
                        if (err) {
                            logger.writeLog('error', null, user.id, err.toString());
                            res.status(500).send({msg: 'Internal Server Error'});
                        }
                        else {
                            /**
                             * When there is a mapping between the device and the new owner, then change device's state
                             * to RESET-STAGE2
                             */
                            if (mapping != null) {
                                device.state = RESETSTAGE2;
                                device.save(null, function (err, savedItem) {
                                    if(err) {
                                        logger.writeLog('error', null, user.id, err.toString());
                                        res.status(500).send({msg: 'Internal Server Error'});
                                    }
                                });
                            }
                            /**
                             * If there is no mapping between the device and the new owner, then create a new device entry
                             * in the able in the state RESET-STAGE2 and create a new mapping between the new created device
                             * and new owner's id.
                             */
                            else {
                                var newDevice = new Device;
                                newDevice.key = device.key;
                                newDevice.allowOffband = device.allowOffband;
                                newDevice.deviceTypeId = device.deviceTypeId;
                                newDevice.state = RESETSTAGE2;
                                newDevice.lastLockTime = device.lastLockTime;
                                newDevice.isDebugInfoRequested = device.isDebugInfoRequested;
                                newDevice.save(null, function (err, savedDevice) {
                                    if (err) {
                                        logger.writeLog('error', null, user.id, err.toString());
                                    }
                                    else {
                                        device.key = null;
                                        device.save(null, function (err, savedItem) {
                                            if(err) {
                                                logger.writeLog('error', null, user.id, err.toString());
                                                res.status(500).send({msg: 'Internal Server Error'});
                                            }
                                        });
                                        var newMapping = new UserDeviceMapping;
                                        newMapping.userId = user.id;
                                        newMapping.deviceId = savedDevice.id;
                                        newMapping.deviceAccessLevel = 'OWNERSHIP';
                                        newMapping.save(null, function (err, savedItem) {
                                            if(err) {
                                                logger.writeLog('error', null, user.id, err.toString());
                                                res.status(500).send({msg: 'Internal Server Error'});
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    })
                }
                else if (err) {
                    logger.writeLog('error', null, user.id, err.toString());
                    res.status(500).send({msg: 'Internal Server Error'});
                }
            })
        }
    })
}
