Steps to run the open-WRT client in development environment
----------------------------------------------------------
1. The 'cloudui' project has a directory 'openwrt-scripts' with all the files needed to run the client in the router.
	- client.ini: The configuration file whose properties have to be modified as needed.
	- client.lua: The client which receives various commands and data from the server and act accordingly.
	- dkjson.lua: This is a Lua library file for processing Json
	- startup.sh: The script that runs the .lua files and adds a necessary firewall rule which needs to be modified as needed.

Transfer the files to the /root/ location of the client: scp * root@192.168.1.1:/root (ip address of the client)

2. The directory 'certs/RouterCerts' has files server.pem, device1.key, device1-cert.pem which are the self-signed server/device certificates. Transfer these files to /root/ of the router. Rename device1.key, device1-cert.pem to device.key, client-cert.pem respectively.

3. Login to the client using ssh: ssh root@192.168.1.1

4. Stunnel should be installed and open the file /etc/stunnel/stunnel.conf. This is the endpoint of the tunnel between client and server through which user-to-server tunnel may be passed.  Modify below lines in that conf file
	client = yes                             
	accept = 192.168.1.1:8080
	connect = 130.233.193.174:8081 (cloud UI IP)
	cert = /root/client-cert.pem
	key = /root/device.key


5. Go to folder root: cd /root/

6. Run the script startup.sh: sh startup.sh
