# Directories and files for the root CA
mkdir root root/certs root/crl root/newcerts root/private
touch root/index.txt # Database
echo "1000" > root/serial # Initial serial number

echo "==============================================="
echo "Generating device root CA private key"
echo "Choose a password for the root key"
echo "======================================="
openssl genrsa -aes256 -out root/private/ca.key.pem 2048

echo "==============================================="
echo "Generating device root CA certificate"
echo "Type in the previously chosen password"
echo "======================================="
openssl req -config root.cnf -key root/private/ca.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out root/certs/ca.cert.pem

# Directories and files for the intermediate CA
mkdir intermediate intermediate/certs intermediate/crl intermediate/newcerts intermediate/private intermediate/csr
touch intermediate/index.txt # Database
echo "1000" > intermediate/serial # Initial serial number

echo "==============================================="
echo "Generating device intermediate CA private key"
echo "Choose a different password for the intermediate key"
echo "==============================================="
openssl genrsa -aes256 -out intermediate/private/intermediate.key.pem 2048

echo "==============================================="
echo "Generating device intermediate CA signing request"
echo "Type in the previously chosen password"
echo "==============================================="
openssl req -config intermediate.cnf -new -sha256 -key intermediate/private/intermediate.key.pem -out intermediate/csr/intermediate.csr.pem

echo "==============================================="
echo "Signing device intermediate CA"
echo "Type in the previously chosen root CA password"
echo "==============================================="
openssl ca -config root.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in intermediate/csr/intermediate.csr.pem -out intermediate/certs/intermediate.cert.pem

# Concatenate the intermediate and root certificates into a single file
cat intermediate/certs/intermediate.cert.pem root/certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem

mkdir device device/private device/csr device/certs
