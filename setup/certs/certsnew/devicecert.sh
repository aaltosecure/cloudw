# Generate a key for the device certificate
openssl genrsa -out device/private/$1.key.pem 2048

# Create a signing request
openssl req -config intermediate.cnf -key device/private/$1.key.pem -new -sha256 -subj "/C=FI/ST=Uusimaa/L=Espoo/O=Aalto Yliopisto/CN=$1" -out device/csr/$1.csr.pem

# Sign the CSR with the intermediate cert
openssl ca -batch -config intermediate.cnf -extensions server_cert -days 3650 -notext -md sha256 -key juujuu -in device/csr/$1.csr.pem -out device/certs/$1.cert.pem

cat device/certs/$1.cert.pem device/private/$1.key.pem >> device/certs/$1.pem
