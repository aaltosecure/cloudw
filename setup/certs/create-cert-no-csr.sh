#!/bin/sh

openssl req -x509 -newkey rsa:2048 -nodes -keyout $1-key.pem -out $1-cert.pem -days 3600
