#!/bin/sh


# Generate a Private Key

openssl genrsa -out $1.key 2048

# Generate a CSR (Certificate Signing Request) 

openssl req -new -key $1.key -out $1-csr.pem

# Generating a Self-Signed Certificate 

openssl x509 -req -in $1-csr.pem -CA deviceRootCA-cert.pem -CAkey deviceRootCA-key.pem -CAcreateserial -out $1-cert.pem -days 500
