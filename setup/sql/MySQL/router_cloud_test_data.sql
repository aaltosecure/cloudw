INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wirelessname','ath0_ssid',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wpawirelesspw','ath0_wpa_psk',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wlnetmode','ath0_net_mode',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wirelesspw','ath0_wpa_psk',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('routername','router_name',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('localip','lan_ipaddr',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('subnetmask','lan_netmask',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('defaultgw','lan_gateway',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('localdns','sv_localdns',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('hostname','wan_hostname',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('domainname','wan_domain',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('mtu','wan_mtu',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('leasedhcp','dhcp_lease',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('sdnsone','wan_dns0_0',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('sdnstwo','wan_dns1_0',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('sdnsthree','wan_dns2_0',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('maxdhcp','dhcp_num',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('dhcpip','dhcp_start',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wlpasswtype','psk',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('ap','ap',2);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('ssidbcast','ath0_closed',2);

INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wirelessname','wireless.@wifi-iface[0].ssid',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wpawirelesspw','wireless.@wifi-iface[0].key',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wlnetmode','wireless.radio0.hwmode',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wirelesspw','wireless.@wifi-iface[0].key',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('routername','system.@system[0].hostname',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('localip','network.lan.ipaddr',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('subnetmask','network.lan.netmask',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('defaultgw','network.lan.gateway',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('localdns','network.lan.dns',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('hostname','system.@system[0].hostname',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('domainname','network.wan.domain',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('mtu','network.wan.mtu',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('leasedhcp','dhcp.lan.leasetime',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('sdnsone','wan_dns0_0',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('sdnstwo','wan_dns1_0',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('sdnsthree','wan_dns2_0',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('maxdhcp','dhcp.lan.limit',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('dhcpip','dhcp.lan.start',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('wlpasswtype','wireless.@wifi-iface[0].encryption',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('ap','wireless.@wifi-iface[0].mode',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('ssidbcast','wireless.@wifi-iface[0].hidden',3);
INSERT INTO `KeyMappings` (`inputName`,`key`,`deviceTypeId`) VALUES ('enablewlan','wireless.radio0.disabled',3);



insert into DeviceType values (1,'hw1','sw1');
insert into DeviceType values (2,'BUFFALO','DDWRT31059');
insert into DeviceType values (3,'BUFFALO','OPENWRT31817');

